<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Auth::routes();


Route::group(['middleware' => ['auth']],function(){
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/', function () {
        return view('layouts.app');
    });

    // USER
    Route::get('/user', 'UserController@index')->name('user.list');
    Route::get('/user/data', 'UserController@getData')->name('user.list.data');
    Route::get('/user/form', 'UserController@form')->name('user.form');
    Route::post('/user/form', 'UserController@submit')->name('user.form.submit');
    Route::post('/user/delete', 'UserController@delete')->name('user.delete');
    Route::get('/user/dropdown/assignee', 'UserController@getDropdown')->name('user.dropdown.assignee');


    // DEPARTMENT
    Route::get('/department/dropdown/data', 'DepartmentController@getDropdown')->name('department.dropdown');
    Route::get('/department', 'DepartmentController@index')->name('department.list');
    Route::get('/department/data', 'DepartmentController@getData')->name('department.list.data');
    Route::get('/department/form', 'DepartmentController@form')->name('department.form');
    Route::post('/department/form', 'DepartmentController@submit')->name('department.form.submit');
    Route::post('/department/delete', 'DepartmentController@delete')->name('department.delete');

//ROLE
    Route::get('/role', 'RoleController@index')->name('role.list');
    Route::get('/role/data', 'RoleController@getData')->name('role.list.data');
    Route::get('/role/form', 'RoleController@form')->name('role.form');
    Route::post('/role/form', 'RoleController@submit')->name('role.form.submit');
    Route::post('/role/delete', 'RoleController@delete')->name('role.delete');

    //MENU
    Route::get('/menu', 'MenuController@index')->name('menu.list');
    Route::get('/menu/data', 'MenuController@getData')->name('menu.list.data');
    Route::get('/menu/form', 'MenuController@form')->name('menu.form');
    Route::post('/menu/form', 'MenuController@submit')->name('menu.form.submit');
    Route::post('/menu/delete', 'MenuController@delete')->name('menu.delete');
    Route::get('/menu/dropdown/data', 'MenuController@getDropdown')->name('menu.dropdown');

    //PERMISSION
    Route::get('/permission', 'PermissionController@index')->name('permission.list');
    Route::get('/permission/data', 'PermissionController@getData')->name('permission.list.data');
    Route::get('/permission/form', 'PermissionController@form')->name('permission.form');
    Route::post('/permission/form', 'PermissionController@submit')->name('permission.form.submit');
    Route::post('/permission/delete', 'PermissionController@delete')->name('permission.delete');
    Route::get('/permission/dropdown_menu/data', 'PermissionController@getDropdownMenu')->name('permission.dropdown_menu');
    Route::get('/permission/dropdown_role/data', 'PermissionController@getDropdownRole')->name('permission.dropdown_role');
    Route::get('/permission/dropdown_show/data', 'PermissionController@getDropdownShow')->name('permission.dropdown_show');
    Route::get('/permission/dropdown_add/data', 'PermissionController@getDropdownAdd')->name('permission.dropdown_add');
    Route::get('/permission/dropdown_edit/data', 'PermissionController@getDropdownEdit')->name('permission.dropdown_edit');
    Route::get('/permission/dropdown_delete/data', 'PermissionController@getDropdownDelete')->name('permission.dropdown_delete');
	
    //JOBDESK ACTIVITY
    Route::get('/jobdesk', 'JobdeskController@index')->name('jobdesk.list');
    Route::get('/jobdesk/data', 'JobdeskController@getData')->name('jobdesk.list.data');
    Route::get('/jobdesk/form', 'JobdeskController@form')->name('jobdesk.form');
    Route::post('/jobdesk/form', 'JobdeskController@submit')->name('jobdesk.form.submit');
    Route::get('/jobdesk/form/{id}/{type}', 'JobdeskController@form')->name('jobdesk.form.edit');
    Route::post('/jobdesk/delete', 'JobdeskController@delete')->name('jobdesk.delete');
    Route::get('/jobdesk/dropdown/data', 'JobdeskController@getDropdown')->name('jobdesk.dropdown');

    //OUTPUT ACTIVITY
    Route::get('/output', 'OutputController@index')->name('output.list');
    Route::get('/output/data', 'OutputController@getData')->name('output.list.data');
    Route::get('/output/form', 'OutputController@form')->name('output.form');
    Route::get('/output/form/change', 'OutputController@formChange')->name('output.form.change');
    Route::post('/output/form/change', 'OutputController@update')->name('output.form.update');
    Route::post('/output/form', 'OutputController@submit')->name('output.form.submit');
    Route::post('/output/data/jobdept', 'OutputController@getByJobDept')->name('output.data.by.jobdept');
    Route::post('/output/delete', 'OutputController@delete')->name('output.delete');
    Route::get('/output/dropdown/data', 'OutputController@getDropdown')->name('jobdesk.dropdown');


    //TASK ACTIVITY
    Route::get('/task', 'TaskController@index')->name('task.list');
    Route::get('/task/data', 'TaskController@getData')->name('task.list.data');
    Route::get('/task/form', 'TaskController@form')->name('task.form');
    Route::post('/task/form', 'TaskController@submit')->name('task.form.submit');

});

//LOGIN USER
Route::get('/auth/login', 'AuthController@login')->name('auth.login');
Route::post('/auth/login', 'AuthController@loginSubmit')->name('auth.login.submit');
Route::get('/auth/logout', 'AuthController@logout')->name('auth.logout');

Route::get('/home', 'HomeController@index')->name('home');
