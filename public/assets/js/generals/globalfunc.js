function show_modal_default(target, index, tipe) {
    var cs = $('meta[name="csrf-token"]').attr("content");
    $.post(target, {
        nilai: index,
        _token: cs
    }, function (mod) {
        $("#modal-show").html(mod);
        // $(".default-select2").select2();

        if($.summernote) {
            console.log('summernote loaded');
        }
        $("#responsive-modal").modal({
            show: true,
            backdrop: true,
            keyboard: true,
        });
    });
}

function get_modal_default(target, index, tipe) {
    var cs = $('meta[name="csrf-token"]').attr("content");
    $.get(target, {
        nilai: index,
        _token: cs,
        tipe: tipe
    }, function (mod) {
        $("#modal-show").html(mod);
        $(".select2").select2();
        if($.summernote) {
            console.log('summernote loaded');

            var is_summernote = document.getElementsByClassName('summernote');
            if (is_summernote.length > 0) {
                $('.summernote').summernote({
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link']],
                        ['view', ['fullscreen', 'codeview', 'help']],
                    ],
                    placeholder: 'Please input output activity description here',
                    height: 200
                });
            }
        }
        validateModalDefault.init();
        $('.get-icon').keyup(function (e) {
            $('.icon-show').html('');
            $('.icon-show').append('<i class="' + $(this).val() + '"></i>');
        });
        $("#responsive-modal").modal({
            show: true
        });
    });
}



function format_no_money(number) {
    if (number !== null) {
        var parts = number.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
}

function unformat_no_money(number) {
    if (number !== null) {
        var parts = number.toString().split(",");
        return parts.join("");
    }
}

$(".only-number").on("keypress keyup blur", function (event) {
    $(this).val(
        $(this)
        .val()
        .replace(/[^\d].+/, "")
    );
    if (event.which < 48 || event.which > 57) {
        event.preventDefault();
    }
});

// price product from ajax
function rp(angka) {
    var rupiah = "";
    var angkarev = angka.toString().split("").reverse().join("");
    for (var i = 0; i < angkarev.length; i++)
        if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ",";
    return (
        "Rp. " +
        rupiah
        .split("", rupiah.length - 1)
        .reverse()
        .join("")
    );
}

// check image product from ajax
function checkImageProduct(img_url) {
    var xhr = new XMLHttpRequest();
    xhr.open("HEAD", img_url, false);
    xhr.send();

    if (xhr.status == "404") {
        console.log("File doesn't exist");
        return $("#baseL").val() + "/assets/images/no-image-available.png";
    } else {
        console.log("File exists");
        return img_url;
    }
}

var validateDefault = (function () {
    var that = {};
    that.init = function () {
        console.log('global funxc was runiing');
        $("#form-default").validate({
            ignore: [],
            errorClass: 'validation-invalid-label',
            successClass: 'validation-valid-label',
            highlight: function (element, errorClass) {
                if ($(element).hasClass('select2')) {
                    $(element).removeClass(errorClass);
                    $(element).next().children().children().addClass('is-invalid').removeClass('is-valid');
                } else {
                    $(element).removeClass(errorClass);
                    $(element).closest('.form-control').addClass('is-invalid').removeClass('is-valid');
                }
            },
            unhighlight: function (element, errorClass) {
                if ($(element).hasClass('select2')) {
                    $(element).removeClass(errorClass);
                    $(element).next().children().children().removeClass('is-invalid').addClass('is-valid');
                } else {
                    $(element).removeClass(errorClass);
                    $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
                }
            },
            errorPlacement: function (error, element) {
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                    if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent().parent().parent());
                    } else {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    }
                } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo(element.parent().parent().parent());
                } else if (element.parents('div').hasClass('has-feedback')) {
                    error.appendTo(element.parent());
                } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('file-input')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                } else if (element.parent().hasClass('select-search')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.hasClass('summernote')) {
                    // error.insertAfter(element);
                    error.insertAfter(element.next('.note-editor'));
                } else {

                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label",
            success: function (label) {
                // label.addClass("validation-valid-label").text("Success.");
            },
            submitHandler: function (form, event) {
                // form.submit();
                event.preventDefault();
                var post_url = $('#form-default').attr("action");
                var request_method = $('#form-default').attr("method");
                var post_data = new FormData($('#form-default')[0]);
                $.ajax({
                    url: post_url,
                    type: request_method,
                    data: post_data,
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    beforeSend: function () {
                        block_page('Data sedang di proses . . ');
                    },
                    success: function (data) {
                        console.log(data);
                        $.unblockUI();
                        if (data.response.code == 200) {


                            // eval(data.response.redirect);
                            if (data.response.redirect != '') {
                                window.location.href = data.response.redirect;
                            } else {
                                window.location.reload();
                            }
                        } else {
                            notif('error', 'Kesalahan', data.response.message);
                        }
                    },
                    error: function (data) {
                        $.unblockUI();
                        notif('error', data.statusText, data.responseJSON.response.message);
                    },
                    complete: function () {

                    }
                });
            }
        });
    }
    return that;
})();

var validateModalDefault = (function () {
    var that = {};
    that.init = function () {
        console.log('global funxc was runiing');
        $("#form-default-modal").validate({
            ignore: 'input[type=hidden], .select2-search__field',
            errorClass: 'invalid-feedback',
            successClass: 'valid-feedback',
            errorElement:'div',
            highlight: function (element, errorClass) {
                $(element).removeClass('is-valid');
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass('is-invalid');
	            $(element).addClass('is-valid');
            },
            errorPlacement: function (error, element) {
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                    if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent().parent().parent());
                    } else {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    }
                } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo(element.parent().parent().parent());
                } else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('file-input')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                } else if (element.parent().hasClass('select-search')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else {
                    error.insertAfter(element);
                }
            },
            validClass: "valid-feedback",
            success: function (label) {
                // label.addClass("validation-valid-label").text("Success.")
            },
            submitHandler: function (form, event) {
                // form.submit();
                event.preventDefault();
                var post_url = $('#form-default-modal').attr("action");
                var request_method = $('#form-default-modal').attr("method");
                var post_data = new FormData($('#form-default-modal')[0]);
                $.ajax({
                    url: post_url,
                    type: request_method,
                    data: post_data,
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    beforeSend: function () {
                        block_page('Data sedang di proses . . ');
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.response.code == 200) {
                            // eval(data.response.redirect);
                            if (data.response.redirect != '') {
                                window.location.href = data.response.redirect;
                            } else {
                                window.location.reload();
                            }
                        } else {
                            $.unblockUI();
                            notif('error', 'Kesalahan', data.response.message);
                        }
                    },
                    error: function (data) {
                        $.unblockUI();
                        console.log(data);
                        // notif('error',data.statusText,data.responseJSON.response.message);
                    },
                    complete: function () {
                        // $.unblockUI();
                    }
                });
            }
        });
    }
    return that;
})();

var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
    '  <div class="modal-content">\n' +
    '    <div class="modal-header align-items-center">\n' +
    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '      <div class="floating-buttons btn-group"></div>\n' +
    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n';

// Buttons inside zoom modal
var previewZoomButtonClasses = {
    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
    fullscreen: 'btn btn-light btn-icon btn-sm',
    borderless: 'btn btn-light btn-icon btn-sm',
    close: 'btn btn-light btn-icon btn-sm'
};

// Icons inside zoom modal classes
var previewZoomButtonIcons = {
    prev: '<i class="icon-arrow-left32"></i>',
    next: '<i class="icon-arrow-right32"></i>',
    toggleheader: '<i class="icon-menu-open"></i>',
    fullscreen: '<i class="icon-screen-full"></i>',
    borderless: '<i class="icon-alignment-unalign"></i>',
    close: '<i class="icon-cross2 font-size-base"></i>'
};

// File actions
var fileActionSettings = {
    zoomClass: '',
    zoomIcon: '<i class="icon-zoomin3"></i>',
    dragClass: 'p-2',
    dragIcon: '<i class="icon-three-bars"></i>',
    removeClass: '',
    removeErrorClass: 'text-danger',
    removeIcon: '<i class="icon-bin"></i>',
    indicatorNew: '<i class="icon-file-plus text-success"></i>',
    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
    indicatorError: '<i class="icon-cross2 text-danger"></i>',
    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
};

function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}

function download_document(id, filename) {
    $.ajax({
        url: $('#baseL').val() + 'document/download',
        type: "get",
        data: {
            id: id
        },
        success: function (response) {
            let resp = JSON.parse(response);
            if (resp.data !== undefined) {
                // console.log('sukses');
                var element = document.createElement('a');
                element.setAttribute('href', resp.data);
                element.setAttribute('download', filename + '.' + resp.extension);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            } else {
                console.log('failed');
            }
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}

function generateAvatarName(name) {
    return name.split(' ').map(function (str) {
        return str ? str[0].toUpperCase() : "";
    }).join('').substring(0, 2);
}

function generateColor() {
    var colors = ["primary", "info", "danger", "warning", "dark", "success"];
    return colors[Math.floor(Math.random() * colors.length)];
}

function generateRandomColor(color) {
    var listColor;
    switch (color) {
        case 'label':
            listColor = ["label-success", "label-primary", "label-danger", "label-info", "label-warning", "label-dark"];
            break;
        case 'symbol':
            listColor = ["symbol-success", "symbol-primary", "symbol-danger", "symbol-info", "symbol-warning", "symbol-dark"];
            break;
        case 'light-symbol':
            listColor = ["symbol-light-success", "symbol-light-primary", "symbol-light-danger", "symbol-light-info", "symbol-light-warning", "symbol-light-dark"];
            break;
        case 'text':
            listColor = ["text-success", "text-primary", "text-danger", "text-info", "text-warning", "text-dark"];
            break;
        default:
            listColor = ["bg-success", "bg-primary", "bg-danger", "bg-info", "bg-warning", "bg-dark"];
            break;
    }
    return listColor[Math.floor(Math.random() * listColor.length)];
}

function generateNotifIcon(activity) {
    switch (activity) {
        case "TICKET_REPLY":
            $icon = 'flaticon-chat';
            break;
        case "TICKET_TIPE":
            $icon = 'flaticon-list';
            break;
        case "TICKET_STATUS":
            $icon = 'fas fa-clipboard-list';
            break;
        case "TICKET_MOVE":
            $icon = 'fas fa-folder-open';
            break;
        default:
            $icon = 'flaticon-exclamation-1';
            break;
    }
    return $icon;
}

// function getNotifTicket() {
//     $.ajax({
//         url: $('#url-base').val() + '/tiket/notif/newticket',
//         type: 'GET',
//         data: null,
//         processData: false,
//         contentType: false,
//         dataType: "JSON",
//         beforeSend: function () {
//             // block_page('Data sedang di proses . . ');
//         },
//         success: function (data) {
//             // $.unblockUI();
//             if (data.response.code == 200) {
//                 // eval(data.response.redirect);
//                 if (data.data.count_ticket > 0) {
//                     $('.new-ticket-list').html('');
//                     $('.new-ticket-count').removeClass('d-none');
//                     $('.new-ticket-count').html(data.data.count_ticket);
//                     $('.new-ticket-counts').html(data.data.count_ticket);
//                     data.data.list_ticket.forEach(ticket => {
//                         $('.new-ticket-list').append('<a href="' + ($('#url-base').val() + '/tiket/detil/' + ticket.ticket_id + '/balas') + '" class="navi-item"><div class="navi-link rounded"><div class="symbol symbol-50 symbol-circle mr-3" title="' + ticket.employee.nama + ' - ' + ticket.employee.office.unitkerja + '"><span class="symbol-label font-weight-bolder text-' + generateColor() + '">' + generateAvatarName(ticket.employee.nama) + '</span></div><div class="navi-text"><div class="font-weight-bold font-size-lg">' + ticket.ticket_title + '</div><div class="d-flex align-items-center justify-content-between flex-wrap"><span class="label label-light-info label-sm label-inline mr-5">#' + ticket.ticket_id + '</span><div class="text-muted font-size-sm" title="Diposting : ' + moment(ticket.created_at).format("dddd, MMMM DD YYYY, HH:mm:ss") + '">' + moment(ticket.created_at).fromNow() + '</div></div></div></div></a>').first();
//                     });
//                 } else {
//                     $('.new-ticket-count').addClass('d-none');
//                 }
//             }
//         },
//         error: function (data) {
//             $.unblockUI();
//             // notif('error',data.statusText,data.responseJSON.response.message);
//         }
//     });
// }

// function getNotificationToMe() {
//     $.ajax({
//         url: $('#url-base').val() + '/notification/list/me',
//         type: 'GET',
//         data: null,
//         processData: false,
//         contentType: false,
//         dataType: "JSON",
//         beforeSend: function () {
//             // block_page('Data sedang di proses . . ');
//         },
//         success: function (data) {
//             // $.unblockUI();
//             if (data.response.code == 200) {
//                 // eval(data.response.redirect);
//                 if (data.data.count_notif > 0) {
//                     $('.notification-list').html('');
//                     $('.notification-count').html(data.data.count_notif);
//                     $('.notification-counts').html(data.data.count_notif);
//                     data.data.list_notification.forEach(notif => {
//                         $('.notification-list').append('<a href="' + ($('#url-base').val() + notif.notification_path) + '" class="navi-item"><div class="navi-link rounded"><div class="symbol symbol-50 symbol-circle mr-3" title="' + notif.employee.nama + ' - ' + notif.employee.office.unitkerja + '"><div class="symbol-label"><i class="' + generateNotifIcon(notif.notification_activity) + ' text-' + generateColor() + ' icon-lg"></i></div></div><div class="navi-text"><div class="font-weight-bold font-size-lg">' + notif.notification_message + '</div><div class="d-flex align-items-center justify-content-between flex-wrap"><div class="text-muted font-size-sm" title="Aktifitas : ' + moment(notif.notification_date).format("dddd, MMMM DD YYYY, HH:mm:ss") + '">' + moment(notif.notification_date).fromNow() + '</div></div></div></div></a>').first();
//                     });
//                 } else {
//                     $('.notification-count').addClass('d-none');
//                     $('.notification-list').html('<div class="font-weight-bold font-size-lg">Belum terdapat notifikasi</div>');
//                 }
//             }
//         },
//         error: function (data) {
//             $.unblockUI();
//             // notif('error',data.statusText,data.responseJSON.response.message);
//         }
//     });
// }

function get_form_export_recap_ticket() {
    var cs = $('meta[name="csrf-token"]').attr("content");
    $.get($('#url-base').val() + '/export/ticket/recap/forum/form', {
        _token: cs
    }, function (mod) {
        $("#modal-show").html(mod);

        $('#kt_datepicker_5').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        $('#export_topic_id').select2({
            placeholder: 'Silahkan pilih topik',
            allowClear: true,
            width: '100%',
            dropdownParent: $('#modal-form-export-recap'),
            'ajax': {
                url: $('#url-base').val() + '/topic/dropdown/data',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    return query;
                },
                processResults: function (data) {
                    var list = $.map(data, function (obj) {
                        obj.id = obj.topic_id;
                        obj.text = obj.topic_title;
                        return obj;
                    });

                    list.push({
                        'id': 'all',
                        'text': 'Seluruhnya',
                        locked: true
                    });

                    return {
                        results: list
                    };
                }
            }
        }).on('select2:select', function (e) {
            $(this).valid();
        });

        $("#form-export-recap-ticket").validate({
            ignore: [],
            errorClass: 'validation-invalid-label',
            successClass: 'validation-valid-label',
            highlight: function (element, errorClass) {
                if ($(element).hasClass('select2')) {
                    $(element).removeClass(errorClass);
                    $(element).next().children().children().addClass('is-invalid').removeClass('is-valid');
                } else {
                    $(element).removeClass(errorClass);
                    $(element).closest('.form-control').addClass('is-invalid').removeClass('is-valid');
                }
            },
            unhighlight: function (element, errorClass) {
                if ($(element).hasClass('select2')) {
                    $(element).removeClass(errorClass);
                    $(element).next().children().children().removeClass('is-invalid').addClass('is-valid');
                } else {
                    $(element).removeClass(errorClass);
                    $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
                }
            },
            errorPlacement: function (error, element) {
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                    if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent().parent().parent());
                    } else {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    }
                } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo(element.parent().parent().parent());
                } else if (element.parents('div').hasClass('has-feedback')) {
                    error.appendTo(element.parent());
                } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('file-input')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                } else if (element.parent().hasClass('select-search')) {
                    error.appendTo(element.parent().parent());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                    error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
                } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.hasClass('summernote')) {
                    // error.insertAfter(element);
                    error.insertAfter(element.next('.note-editor'));
                } else {

                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label",
            success: function (label) {
                // label.addClass("validation-valid-label").text("Success.");
            },
            submitHandler: function (form, event) {
                // form.submit();
                event.preventDefault();
                var post_url = $('#form-export-recap-ticket').attr("action");
                var request_method = $('#form-export-recap-ticket').attr("method");
                // console.log($("input[name=export_tipe]:checked").val());
                // console.log($('#export_periode_start').val());
                // console.log($('#export_periode_end').val());
                // return;
                // $.ajax({
                //     url : $('#url-base').val() + '/export/pdf/ticket/recap/forum',
                //     type: 'POST',
                //     data : {
                //         _token: $('meta[name="csrf-token"]').attr("content"),
                //         export_topic_id: $('#export_topic_id').val()
                //     },
                //     beforeSend: function(){
                //         block_page('Sedang mengambil data..');
                //     },
                //     success: function(data) {
                //        console.log(data);
                //        $.unblockUI();
                //     },
                //     error: function(data){
                //         $.unblockUI();
                //         notif('error',data.statusText,data.responseJSON.response.message);

                //     },
                //     complete: function() {

                //     }
                // });
                $.ajax({
                    xhrFields: {
                        responseType: 'blob',
                    },
                    type: $('#form-export-recap-ticket').attr("method"),
                    url: $("input[name=export_tipe]:checked").val() == 'pdf' ? $('#url-base').val() + '/export/pdf/ticket/recap/forum' : $('#url-base').val() + '/export/excel/ticket/recap/forum',
                    data: {
                        export_topic_id: $('#export_topic_id').val(),
                        date_start: $('#export_periode_start').val(),
                        date_end: $('#export_periode_end').val()
                    },
                    responseType: 'blob',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        block_page('Data sedang di proses . . ');
                    },
                    success: function (data, status, xhr) {
                        var disposition = xhr.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        console.log(matches);
                        if ($("input[name=export_tipe]:checked").val() == 'pdf') {
                            var blob = new Blob([data], {
                                type: 'application/pdf'
                            });
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = 'Rekap-Open-Tiket.pdf';
                        } else {
                            var blob = new Blob([data], {
                                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                            });
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = 'Rekap-Open-Tiket.xlsx';
                        }

                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    },
                    error: function (xhr) {
                        notif('warning', 'Peringatan', xhr.responseJSON.response.message);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        $("#modal-form-export-recap").modal({
            show: true
        });
    });
}

function getFirstForum(topic_ref_id) {
    $.ajax({
        url: $('#url-base').val() + '/topic/forum/getfirst',
        type: 'POST',
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: {
            topic_ref_id: topic_ref_id,
        },
        dataType: "JSON",
        beforeSend: function () {
            block_page('Data sedang di proses . . ');
        },
        success: function (data) {
            if (data.response.code == 200) {
                window.location.href = $('#url-base').val() + '/tiket/list/forum/' + data.data.topic_meta;
            } else {
                $.unblockUI();
                notif('error', 'Kesalahan', data.response.message);
            }
        },
        error: function (data) {
            $.unblockUI();
            console.log(data);
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

let employeeActive = null;

// $(document).ready(function () {
//     if (!$('#topbar-item-ticket').hasClass('d-none')) {
//         getNotifTicket();
//     }
// });
