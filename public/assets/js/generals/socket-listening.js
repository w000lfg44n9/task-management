

    $(document).ready(function() {
        if (employeeActive !== null) {
            Echo.private('App.Models.Employee.'+employeeActive.userid)
                .notification((notification) => {
                    notif(`info`,'Notifikasi Baru',notification.message);
                    getNotificationToMe();
            });

            if (employeeActive.kantorid == 0) {
                Echo.private('ticket.new')
                    .listen('NewTicketNotification', (e) => {
                        notif(`info`,e.ticket.created_by+' memposting tiket baru !',e.ticket.ticket_title);
                        getNotifTicket();
                });
            }

            // if (employeeActive !== null) {
                getNotificationToMe();
            // }
        }


    });
