function block_page(msg) {
    $.blockUI({
        message:
            '<i class="fas fa-spin fa-sync text-white"></i><br><span class="text-semibold text-white">' +
            msg +
            "</span>",
        overlayCSS: {
            backgroundColor: "#000",
            opacity: 0.5,
            cursor: "wait",
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "transparent",
            zIndex: 999999,
        },
        baseZ: 999999,
    });
}

function block_element(element, msg) {
    var block_ele = $(element);
    $(block_ele).block({
        message:
            '<i class="fas fa-spin fa-sync text-white"></i><br><span class="text-semibold text-white">' +
            msg +
            "</span>",
        timeout: 2000, //unblock after 2 seconds
        overlayCSS: {
            backgroundColor: "#000",
            opacity: 0.5,
            cursor: "wait",
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "transparent",
            zIndex: 999999,
        },
        baseZ: 999999,
    });
}

function notif(tipe, head, msg) {
    // if (!$().toastr) {
    //     console.warn('Warning - toastr.min.js is not loaded.');
    //     return;
    // }
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };


    switch (tipe) {
        case "info":
            toastr.info(msg, head);
            break;
        case "success":
            toastr.success(msg, head);
            break;
        case "warning":
            toastr.warning(msg, head);
            break;
        case "error":
            toastr.error(msg, head);
            break;
        default:
            toastr.info(msg, head);
            break;
    }
}

function pops_timer(tipe, judul, pesan, aksi = null) {
    Swal.fire({
        title: judul,
        text: pesan,
        icon: tipe,
        timer: 3000,
        showConfirmButton: false,
    }).then((result) => {
        if (result.dismiss === swal.DismissReason.timer) {
            // Swal(
            //   'Deleted!',
            //   'Your file has been deleted.',
            //   'success'
            // )
            console.log("euy");
            if (aksi != null) {
                eval(aksi);
            }
        }
    });
}

function pops_question(judul, pesan, aksi) {
    Swal.fire({
        title: judul,
        text: pesan,
        icon: "question",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-sm btn-info",
        cancelButtonClass: "btn btn-sm btn-danger mg-l-20",
        buttonsStyling: false,
    }).then((result) => {
        if (result.value) {
            console.log("euy");
            eval(aksi);
        }
    });
}

function konfirm(judul, pesan, aksi, val) {
    Swal.fire({
        title: judul,
        text: pesan,
        icon: "question",
        showCancelButton: true,
        confirmButtonText: judul,
        cancelButtonText: "Tutup",
        confirmButtonClass: "btn btn-sm btn-info",
        cancelButtonClass: "btn btn-sm btn-danger m-l-20",
        buttonsStyling: false,
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: aksi,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
                data: { id: val },
                dataType: "JSON",
                beforeSend: function () {
                    block_page('Data sedang di proses . . ');
                },
                success: function (data) {
                    $.unblockUI();
                    if (data.response.code == 200) {
                        Swal.fire({
                            title: "Berhasil",
                            text: data.response.message,
                            confirmButtonClass: "btn btn-info btn-sm",
                            icon: "success",
                            buttonsStyling: false,
                        }).then((result_berhasil) => {
                            if (result_berhasil.value) {
                                if (data.response.redirect != null) {
                                    window.location.href=data.response.redirect;
                                } else {
                                    setTimeout("location.reload(true);", 1000);
                                }
                            }
                        });
                    } else {
                        Swal.fire({
                            title: "Peringatan",
                            text: data.response.message,
                            confirmButtonClass: "btn btn-danger btn-sm",
                            icon: "error",
                            buttonsStyling: false,
                        }).then((result_gagal) => {
                            if (result_gagal.value) {
                                if (data.response.redirect != null) {
                                    window.location.href=data.response.redirect;
                                } else {
                                    setTimeout("location.reload(true);", 1000);
                                }
                            }
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                    //   Swal({
                    //       title: "Gagal !",
                    //       text: "Proses gagal !",
                    //       confirmButtonClass: 'btn btn-danger btn-sm',
                    //       icon: "error",
                    //       buttonsStyling: false
                    //   });
                    //   setTimeout("location.reload(true);",1000);
                    $.unblockUI();
                },
                complete: function () {
                    // $.unblockUI();
                },
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
            Swal.fire({
                title: "Tutup !",
                text: "Proses dibatalkan !",
                confirmButtonClass: "btn btn-danger btn-sm",
                icon: "error",
                buttonsStyling: false,
            });
        }
    });
}
