"use strict";
var table;
var KTDatatablesAdvancedColumnRendering = function () {

    var init = function () {

        table = $('#kt_datatable').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Masukan pencarian...',
                lengthMenu: '<span>Tampilkan:</span> _MENU_',
                paginate: { 'first': 'Pertama', 'last': 'Terakhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
                processing: '<i class="fas fa-spinner text-danger fa-spin fa-1x fa-fw"></i> Sedang mengambil data..'
            },
            serverSide: true,
            ajax: {
                url: $('#url-base').val() + '/role/data',
                dataType: "json",
                type: "GET",
            },
            "columnDefs": [
                { "width": "5%", "targets": 0 },
                { "width": "60%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "15%", "targets": 3 },
                { "width": "5%", "targets": 4 },
            ],
            columns: [
                {
                    title: '*',
                    data: 'role_id',
                    name: 'role_id',
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    sClass: 'text-center',
                },
                {
                    title: 'Role Name',
                    data: 'role_name',
                    name: 'role_name'
                },
                {
                    title: 'Status',
                    data: 'status.status_name',
                    name: 'status.status_name',
                },
                {
                    title: 'Diperbarui',
                    data: 'created_by',
                    name: 'created_by',
                    'render': function (data, type, row, meta) {
                        return '<span style="width: 158px;"><div class="font-size-md text-success mb-0">' + row.created_at + '</div><div class="text-muted">' + (data == null ? '-' : data) + '</div></span>';
                    },
                },
                {
                    title: 'Act.',
                    data: 'action',
                    name: 'action',
                    sClass: 'text-center'
                }
            ],
            order: [
                [1, 'asc']
            ]
        });

        $('#kt_datatable_search_status').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {

        //main function to initiate the module
        init: function () {
            init();
        }
    };
}();

jQuery(document).ready(function () {
    KTDatatablesAdvancedColumnRendering.init();
});

function get_modal_user(target, index, tipe) {
    var cs = $('meta[name="csrf-token"]').attr("content");
    $.get(target, { nilai: index, _token: cs, tipe: tipe }, function (mod) {
        $("#modal-show").html(mod);
        $(".only-number").on("keypress keyup blur", function (event) {
            $(this).val(
                $(this)
                    .val()
                    .replace(/[^\d].+/, "")
            );
            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            }
        });
        $('#user_date_birth').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            format: 'yyyy-mm-dd',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#role_id').select2({
            placeholder: 'Pick role user',
            width: '100%',
            allowClear: true,
            'ajax': {
                url: $('#url-base').val() + '/role/dropdown/data',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    return query;
                },
                processResults: function (data) {
                    var list = $.map(data, function (obj) {
                        obj.id = obj.role_id;
                        obj.text = obj.role_name;
                        return obj;
                    });

                    return {
                        results: list
                    };
                }
            }
        }).on('select2:select', function (e) {
            $(this).valid();
        });


        var avatar5 = new KTImageInput('kt_image_5');

        avatar5.on('cancel', function (imageInput) {
            swal.fire({
                title: 'Image successfully changed !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
        });

        avatar5.on('change', function (imageInput) {
            swal.fire({
                title: 'Image successfully changed !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
        });

        avatar5.on('remove', function (imageInput) {
            swal.fire({
                title: 'Image successfully removed !',
                type: 'error',
                buttonsStyling: false,
                confirmButtonText: 'Got it!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
        });


        validateModalDefault.init();
        $("#responsive-modal").modal({ show: true });
    });
}
