let ticket_attachment_list = [];
let ticket_attachment_list_removed = [];
let ticket_attachment_name = [];

jQuery(document).ready(function () {
    $('.select2').select2();
    $('.summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        placeholder: 'Please input task description here',
        height: 200
    });

    $('#jobact_id').select2({
        placeholder: 'Please pick jobdesk activity',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/jobdesk/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.jobact_id;
                    obj.text = obj.jobact_title;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
    });

    $('#department_id').select2({
        placeholder: 'Pick department',
        width: '100%',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/department/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.department_id;
                    obj.text = obj.department_name;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
    });

    $('#output_id').select2({
        placeholder: 'Pick output activity',
        width: '100%',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/output/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    jobact_id: $('#jobact_id').val(),
                    dept_id: $('#department_id').val()
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.output_id;
                    obj.text = obj.output_title;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
    });

    var $validate_output = $("#form-task").validate({
        ignore: 'input[type=hidden], .select2-search__field, :hidden:not(.summernote),.note-editable.card-block',
        errorClass: 'invalid-feedback',
        successClass: 'valid-feedback',
        errorElement: 'div',
        highlight: function (element, errorClass) {
            $(element).removeClass('is-valid');
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        },
        errorPlacement: function (error, element) {
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('file-input')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('select-search')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            } else if (element.hasClass("summernote")) {
                error.insertAfter(element.siblings(".note-editor"));
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "valid-feedback",
        success: function (label) {
            // label.addClass("validation-valid-label").text("Success.")
        },
        submitHandler: function (form, event) {
            // form.submit();
            event.preventDefault();
            var post_url = $('#form-task').attr("action");
            var request_method = $('#form-task').attr("method");
            var post_data = new FormData($('#form-task')[0]);
            post_data.append('ticket_attachment', JSON.stringify(ticket_attachment_list));
            post_data.append('ticket_attachment_removed', JSON.stringify(ticket_attachment_list_removed));

            $.ajax({
                url: post_url,
                type: request_method,
                data: post_data,
                processData: false,
                contentType: false,
                dataType: "JSON",
                beforeSend: function () {
                    block_page('Data sedang di proses . . ');
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();
                    if (data.response.code == 200) {
                        if (data.response.redirect != '') {
                            window.location.href = data.response.redirect;
                        } else {
                            window.location.reload();
                        }
                    } else {
                        notif('error', 'Kesalahan', data.response.message);
                    }
                },
                error: function (data) {
                    $.unblockUI();
                    notif('error', data.statusText, data.responseJSON.response.message);
                },
                complete: function () {

                }
            });
        }
    });

    $('#task_start_date').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format: 'yyyy-mm-dd',
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('#task_due_date').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format: 'yyyy-mm-dd',
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('#ticket_attachment').dropzone({
        url: "target-url",
        paramName: "file",
        maxFiles: 5,
        maxFilesize: 2,
        autoProcessQueue: false,
        autoQueue: false,
        addRemoveLinks: true,
        acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.rar,.zip",
        init: function () {
            var myDropzone = this;
            this.on('addedfile', function (file) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    let baseImageSplit = event.target.result.split(',');
                    ticket_attachment_list.push({
                        'filetype': baseImageSplit[0],
                        'base64': baseImageSplit[1],
                        'filename': file.name
                    });
                };
                reader.readAsDataURL(file);
            });
            this.on('removedfile', function (file) {
                if (file.dataURL == undefined) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        let baseImageSplit = event.target.result.split(',');
                        // idx = ticket_attachment_list.indexOf(baseImageSplit[1]);
                        var idx = ticket_attachment_list.findIndex(x => x.filename === file.name);
                        ticket_attachment_list.splice(idx, 1);

                    };
                    reader.readAsDataURL(file);

                } else {
                    let baseImageSplit = file.dataURL.split(',');
                    // idx = ticket_attachment_list.indexOf(baseImageSplit[1]);
                    var idx = ticket_attachment_list.findIndex(x => x.filename === file.name);
                    ticket_attachment_list.splice(idx, 1);
                }

            });
        }
    });

    var input = document.querySelector('input[id=task_assignee_to]'),
        tagify = new Tagify(input, {
            delimiters: ", ",
            transformTag: function (tagData) {
                tagData.class = 'tagify__tag tagify__tag--primary';
            },
            enforceWhitelist: true,
            autocomplete: true,
            // maxTags: 5,
            // whitelist:[],
            templates: {
                dropdownItem: function (tagData) {
                    try {
                        var html = '';
                        html += '<div class="tagify__dropdown__item">';
                        html += '   <div class="d-flex align-items-center">';
                        html += '       <span class="symbol symbol-default mr-2">';
                        html += '           <span class="symbol-label" >' + (tagData.initial ? tagData.initial : '-') + '</span>';
                        html += '       </span>';
                        html += '       <div class="d-flex flex-column">';
                        html += '           <a href="#" class="text-dark-75 text-hover-primary font-weight-bold">' + (tagData.value ? tagData.value : '') + '</a>';
                        html += '           <span class="text-muted font-weight-bold">' + (tagData.department ? tagData.department : '-') + '</span>';
                        html += '       </div>';
                        html += '   </div>';
                        html += '</div>';

                        return html;
                    } catch (err) {
                        console.log(err);
                    }
                }
            },
            dropdown: {
                enabled: 1, // suggest tags after a single character input
                classname: "color-blue",
                // classname : 'extra-properties', // custom class for the suggestions dropdown
                // itemTemplate : function(tagData){
                //     // return `<div class='tagify__dropdown__item ${tagData.class ? tagData.class : ""}'>

                //     //             <span>${tagData.value}</span>
                //     //         </div>`

                // }
            },
            mapValueToProp: "code",
        }),
        controller;
    tagify.on('input', onInput);
    function onInput(e) {
        var value = e.detail.value;
        tagify.settings.whitelist.length = 0;
        controller && controller.abort();
        controller = new AbortController();

        fetch($('#url-base').val() + '/user/dropdown/assignee?search=' + value)
            .then(RES => RES.json())
            .then(function (res) {
                console.log(res);
                var values = []
                $.each(res, function (value, display) {
                    var client = {
                        code: display['user_id'],
                        value: display['user_name'],
                        department: (display['department'] === null) ? '-' : display['department'].department_name,
                        initial: display['initial']
                    }
                    values.push(client)
                })

                values = JSON.stringify(values)
                values = JSON.parse(values)
                tagify.settings.whitelist = values;
                tagify.dropdown.show.call(tagify, value); // render the suggestions dropdown
            })
    }
});

function removeAttachment(path, idx) {
    // console.log(path);
    // console.log(idx);
    // return 'remove attachment';
    Swal.fire({
        title: "Hapus lampiran",
        text: "Yakin hapus lampiran ini ?",
        icon: "question",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        cancelButtonText: "Tutup",
        confirmButtonClass: "btn btn-sm btn-info",
        cancelButtonClass: "btn btn-sm btn-danger m-l-20",
        buttonsStyling: false,
    }).then((result) => {
        if (result.value) {
            ticket_attachment_list_removed.push(path);
            $('.ticket-attachment-' + idx).html('');
        } else if (result.dismiss === swal.DismissReason.cancel) {
            Swal.fire({
                title: "Tutup !",
                text: "Proses dibatalkan !",
                confirmButtonClass: "btn btn-danger btn-sm",
                icon: "error",
                buttonsStyling: false,
            });
        }
    });
}

function remove_output(idx) {
    data_output_table.splice(idx, 1);
    output_table.clear();
    output_table.rows.add(data_output_table);
    output_table.draw();
}

function change_output(idx) {
    let data_output = data_output_table[idx];
    $('#btn-add-output').html('<i class="flaticon2-sheet"></i> Change Output');
    $('#output_id').val(idx);
    $('#output_title').val(data_output.output_title);
    $('#output_desc').summernote("code", data_output.output_desc);
    $('#output_title').focus();
}

function get_list_by_job_dept() {
    $.ajax({
        url: $('#url-base').val() + '/output/data/jobdept',
        type: 'POST',
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            ),
        },
        data: {
            '_token': $('meta[name="csrf-token"]').attr(
                "content"
            ),
            'jobact_id': $('#jobact_id').val(),
            'department_id': $('#department_id').val()
        },
        dataType: "JSON",
        beforeSend: function () {
            block_page('Data sedang di proses . . ');
        },
        success: function (data) {
            $.unblockUI();
            if (data.response.code == 200) {
                data_output_table = (data.data.length > 0) ? data.data.map(opt => {
                    return {
                        "output_id": opt.output_id,
                        "output_title": opt.output_title,
                        "output_desc": opt.output_desc
                    }
                }) : [];
                output_table.clear();
                output_table.rows.add(data_output_table);
                output_table.draw();
            } else {
                notif('error', 'Kesalahan', data.response.message);
            }
        },
        error: function (data) {
            $.unblockUI();
            notif('error', data.statusText, data.responseJSON.response.message);
        },
        complete: function () {

        }
    });
}
