"use strict";
var table;
var KTDatatablesAdvancedColumnRendering = function () {

    var init = function () {

        table = $('#kt_datatable').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Masukan pencarian...',
                lengthMenu: '<span>Tampilkan:</span> _MENU_',
                paginate: { 'first': 'Pertama', 'last': 'Terakhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
                processing: '<i class="fas fa-spinner text-danger fa-spin fa-1x fa-fw"></i> Sedang mengambil data..'
            },
            serverSide: true,
            ajax: {
                url: $('#url-base').val() + '/output/data',
                dataType: "json",
                type: "GET",
            },
            "columnDefs": [
                { "width": "5%", "targets": 0 },
                { "width": "30%", "targets": 1 },
                { "width": "25%", "targets": 2 },
                { "width": "25%", "targets": 3 },
                { "width": "5%", "targets": 4 },
            ],
            columns: [
                {
                    title: '*',
                    data: 'output_id',
                    name: 'output_id',
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    sClass: 'text-center',
                },
                {
                    title: 'Title',
                    data: 'output_title',
                    name: 'output_title',
                },
                {
                    title: 'Jobdesk',
                    data: 'jobact.jobact_title',
                    name: 'jobact.jobact_title',
                },
                {
                    title: 'Department',
                    data: 'department.department_name',
                    name: 'department.department_name',
                },
                {
                    title: 'Status',
                    data: 'status.status_name',
                    name: 'status.status_name',
                },
                {
                    title: 'Aksi',
                    data: 'action',
                    name: 'action',
                    sClass: 'text-center'
                }
            ],
            order: [
                [1, 'asc']
            ]
        });

        $('#kt_datatable_search_status').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {

        //main function to initiate the module
        init: function () {
            init();
        }
    };
}();

jQuery(document).ready(function () {
    KTDatatablesAdvancedColumnRendering.init();
});
