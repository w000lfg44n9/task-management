let ticket_attachment_list = [];
let ticket_attachment_list_removed = [];
let ticket_attachment_name = [];
var output_table;
var data_output_table = [];
jQuery(document).ready(function () {
    $('.select2').select2();
    $('.summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        placeholder: 'Please input output activity description here',
        height: 200
    });

    $('#jobact_id').select2({
        placeholder: 'Please pick jobdesk activity',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/jobdesk/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.jobact_id;
                    obj.text = obj.jobact_title;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
        if ($('#department_id').val()) {
            get_list_by_job_dept();
        }
    });

    $('#department_id').select2({
        placeholder: 'Pick department output',
        width: '100%',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/department/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.department_id;
                    obj.text = obj.department_name;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
        if ($('#jobact_id').val()) {
            get_list_by_job_dept();
        }
    });

    $('#ticket_behalf_of').select2({
        placeholder: 'Silahkan pilih pegawai yang diwakilkan',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/employee/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.username;
                    obj.text = obj.nama.trim() + ' - ' + (obj.office == null ? '' : obj.office.unitkerja);
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    });

    var $validate_output = $("#form-output").validate({
        ignore: 'input[type=hidden], .select2-search__field, :hidden:not(.summernote),.note-editable.card-block',
        errorClass: 'invalid-feedback',
        successClass: 'valid-feedback',
        errorElement: 'div',
        highlight: function (element, errorClass) {
            $(element).removeClass('is-valid');
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        },
        errorPlacement: function (error, element) {
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('file-input')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('select-search')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            } else if (element.hasClass("summernote")) {
                error.insertAfter(element.siblings(".note-editor"));
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "valid-feedback",
        success: function (label) {
            // label.addClass("validation-valid-label").text("Success.")
        },
        submitHandler: function (form, event) {
            // form.submit();
            event.preventDefault();
            if (data_output_table.length < 1) {
                pops_timer('warning', 'Warning', 'At leats add one output before submit this form', null);
                $('#output_title').focus();
                return;
            }
            var post_url = $('#form-output').attr("action");
            var request_method = $('#form-output').attr("method");
            var post_data = new FormData($('#form-output')[0]);
            post_data.append('output_list', JSON.stringify(data_output_table));
            console.log('hereee');
            $.ajax({
                url: post_url,
                type: request_method,
                data: post_data,
                processData: false,
                contentType: false,
                dataType: "JSON",
                beforeSend: function () {
                    block_page('Data sedang di proses . . ');
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();
                    if (data.response.code == 200) {
                        if (data.response.redirect != '') {
                            window.location.href = data.response.redirect;
                        } else {
                            window.location.reload();
                        }
                    } else {
                        notif('error', 'Kesalahan', data.response.message);
                    }
                },
                error: function (data) {
                    $.unblockUI();
                    notif('error', data.statusText, data.responseJSON.response.message);
                },
                complete: function () {

                }
            });
        }
    });

    output_table = $('#output-list-table').DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        destroy: true,
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Masukan pencarian...',
            lengthMenu: '<span>Tampilkan:</span> _MENU_',
            paginate: { 'first': 'Pertama', 'last': 'Terakhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
            processing: '<i class="fas fa-spinner text-danger fa-spin fa-1x fa-fw"></i> Sedang mengambil data..'
        },
        data: data_output_table,
        "columnDefs": [
            { "width": "5%", "targets": 0 },
            { "width": "40%", "targets": 1 },
            { "width": "40%", "targets": 2 },
            { "width": "5%", "targets": 3 },
        ],
        columns: [
            {
                title: '*',
                data: 'output_title',
                name: 'output_title',
                "render": function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                sClass: 'text-center',
            },
            {
                title: 'Title',
                data: 'output_title',
                name: 'output_title',
            },
            {
                title: 'Description',
                data: 'output_desc',
                name: 'output_desc',
            },
            {
                title: 'Act',
                data: 'output_title',
                name: 'output_title',
                sClass: 'text-center',
                "render": function (data, type, row, meta) {
                    return '<div class="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups"> <div class="btn-group" role="group" aria-label="First group"> <button type="button" onclick="change_output(`' + (meta.row + meta.settings._iDisplayStart) + '`)" class="btn btn-sm btn-primary  btn-icon"><i class="la la-file-text-o"></i></button> <button type="button" onclick="remove_output(`' + (meta.row + meta.settings._iDisplayStart) + '`)" class="btn btn-sm btn-danger btn-icon"><i class="la la-scissors"></i></button> </div> </div>';
                },
            }
        ],
        order: [
            [0, 'asc']
        ]
    });

    $('#kt_datatable_search_status').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'Status');
    });

    $('#kt_datatable_search_type').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    $('#btn-add-output').click(function () {
        var is_valid = true;
        if (!$('#output_title').val()) {
            $validate_output.showErrors({
                'output_title': 'This field is required'
            });
            is_valid = false;
        }

        if (!$('#output_desc').val()) {
            $validate_output.showErrors({
                'output_desc': 'This field is required'
            });
            is_valid = false;
        }

        if (is_valid) {
            if ($('#output_id').val()) {
                let data_output = data_output_table[$('#output_id').val()];
                    data_output.output_title = $('#output_title').val();
                    data_output.output_desc = $('#output_desc').val();
            }else{
                data_output_table.push({
                    "output_id" : null,
                    "output_title": $('#output_title').val(),
                    "output_desc": $('#output_desc').val(),
                });
            }

            $('#output_title').val('');
            $('#output_desc').summernote("code", '');

            output_table.clear();
            output_table.rows.add(data_output_table);
            output_table.draw();

            $('#btn-add-output').html('<i class="flaticon-file-1"></i> Add output');
        }
    });
});

function removeAttachment(path, idx) {
    Swal.fire({
        title: "Hapus lampiran",
        text: "Yakin hapus lampiran ini ?",
        icon: "question",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        cancelButtonText: "Tutup",
        confirmButtonClass: "btn btn-sm btn-info",
        cancelButtonClass: "btn btn-sm btn-danger m-l-20",
        buttonsStyling: false,
    }).then((result) => {
        if (result.value) {
            ticket_attachment_list_removed.push(path);
            $('.ticket-attachment-' + idx).html('');
        } else if (result.dismiss === swal.DismissReason.cancel) {
            Swal.fire({
                title: "Tutup !",
                text: "Proses dibatalkan !",
                confirmButtonClass: "btn btn-danger btn-sm",
                icon: "error",
                buttonsStyling: false,
            });
        }
    });
}

function remove_output(idx) {
    data_output_table.splice(idx, 1);
    output_table.clear();
    output_table.rows.add(data_output_table);
    output_table.draw();
}

function change_output(idx) {
    let data_output = data_output_table[idx];
    $('#btn-add-output').html('<i class="flaticon2-sheet"></i> Change Output');
    $('#output_id').val(idx);
    $('#output_title').val(data_output.output_title);
    $('#output_desc').summernote("code",data_output.output_desc);
    $('#output_title').focus();
}

function get_list_by_job_dept() {
    $.ajax({
        url: $('#url-base').val()+'/output/data/jobdept',
        type: 'POST',
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            ),
        },
        data: {
            '_token' : $('meta[name="csrf-token"]').attr(
                "content"
            ),
            'jobact_id' : $('#jobact_id').val(),
            'department_id' : $('#department_id').val()
        },
        dataType: "JSON",
        beforeSend: function () {
            block_page('Data sedang di proses . . ');
        },
        success: function (data) {
            $.unblockUI();
            if (data.response.code == 200) {
                data_output_table = (data.data.length > 0) ? data.data.map(opt => {
                    return {
                        "output_id" : opt.output_id,
                        "output_title" : opt.output_title,
                        "output_desc" : opt.output_desc
                    }
                }) : [];
                output_table.clear();
                output_table.rows.add(data_output_table);
                output_table.draw();
            } else {
                notif('error', 'Kesalahan', data.response.message);
            }
        },
        error: function (data) {
            $.unblockUI();
            notif('error', data.statusText, data.responseJSON.response.message);
        },
        complete: function () {

        }
    });
}
