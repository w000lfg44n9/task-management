let ticket_attachment_list = [];
let ticket_attachment_list_removed = [];
let ticket_attachment_name = [];

jQuery(document).ready(function () {
    $('.select2').select2();
    $('.summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        placeholder: 'Please input jodbesk activity note here',
        height: 300
    });

    $('#jobact_reg_at').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format: 'yyyy-mm-dd',
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('#jobact_finish_at').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format: 'yyyy-mm-dd',
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $('#topic_ref_id').select2({
        placeholder: 'Silahkan pilih topik',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/topic/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.topic_id;
                    obj.text = obj.topic_title;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
        $('#topic_id').select2({
            placeholder: 'Silahkan pilih forum',
            allowClear: true,
            'ajax': {
                url: $('#url-base').val() + '/topic/forum/dropdown/data',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        topic_ref_id: e.target.value
                    }
                    return query;
                },
                processResults: function (data) {
                    var list = $.map(data, function (obj) {
                        obj.id = obj.topic_id;
                        obj.text = obj.topic_title;
                        return obj;
                    });

                    return {
                        results: list
                    };
                }
            }
        }).on('select2:select', function (e) {
            $(this).valid();
            $('#cat_id').select2({
                placeholder: 'Silahkan pilih kategori tiket',
                allowClear: true,
                'ajax': {
                    url: $('#url-base').val() + '/category/dropdown/data',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            topic_id: e.target.value
                        }
                        return query;
                    },
                    processResults: function (data) {
                        var list = $.map(data, function (obj) {
                            obj.id = obj.category_id;
                            obj.text = obj.category_name;
                            return obj;
                        });

                        return {
                            results: list
                        };
                    }
                }
            }).on('select2:select', function (e) {
                $(this).valid();
            });
        });
    });

    $('#topic_id').select2({
        placeholder: 'Silahkan pilih forum',
        allowClear: true,
    });

    $('#type_id').select2({
        placeholder: 'Silahkan pilih tipe tiket',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/type/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.type_id;
                    obj.text = obj.type_name;
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    }).on('select2:select', function (e) {
        $(this).valid();
    });

    $('#ticket_behalf_of').select2({
        placeholder: 'Silahkan pilih pegawai yang diwakilkan',
        allowClear: true,
        'ajax': {
            url: $('#url-base').val() + '/employee/dropdown/data',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                var list = $.map(data, function (obj) {
                    obj.id = obj.username;
                    obj.text = obj.nama.trim() + ' - ' + (obj.office == null ? '' : obj.office.unitkerja);
                    return obj;
                });

                return {
                    results: list
                };
            }
        }
    });

    $('#ticket_attachment').dropzone({
        url: "target-url",
        paramName: "file",
        maxFiles: 5,
        maxFilesize: 2,
        autoProcessQueue: false,
        autoQueue: false,
        addRemoveLinks: true,
        acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.rar,.zip",
        init: function () {
            var myDropzone = this;
            this.on('addedfile', function (file) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    let baseImageSplit = event.target.result.split(',');
                    ticket_attachment_list.push({
                        'filetype': baseImageSplit[0],
                        'base64': baseImageSplit[1],
                        'filename': file.name
                    });
                };
                reader.readAsDataURL(file);
            });
            this.on('removedfile', function (file) {
                if (file.dataURL == undefined) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        let baseImageSplit = event.target.result.split(',');
                        // idx = ticket_attachment_list.indexOf(baseImageSplit[1]);
                        var idx = ticket_attachment_list.findIndex(x => x.filename === file.name);
                        ticket_attachment_list.splice(idx, 1);

                    };
                    reader.readAsDataURL(file);

                } else {
                    let baseImageSplit = file.dataURL.split(',');
                    // idx = ticket_attachment_list.indexOf(baseImageSplit[1]);
                    var idx = ticket_attachment_list.findIndex(x => x.filename === file.name);
                    ticket_attachment_list.splice(idx, 1);
                }

            });
        }
    });

    $("#form-jobdesk").validate({
        ignore: 'input[type=hidden], .select2-search__field',
        errorClass: 'invalid-feedback',
        successClass: 'valid-feedback',
        errorElement: 'div',
        highlight: function (element, errorClass) {
            $(element).removeClass('is-valid');
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        },
        errorPlacement: function (error, element) {
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            } else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('file-input')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('select-search')) {
                error.appendTo(element.parent().parent());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'radio')) {
                error.appendTo(element.parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('form-check-input-styled') && (element[0].type == 'checkbox')) {
                error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().last());
            } else if (element.hasClass('select2') && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "valid-feedback",
        success: function (label) {
            // label.addClass("validation-valid-label").text("Success.")
        },
        submitHandler: function (form, event) {
            // form.submit();
            event.preventDefault();
            console.log(ticket_attachment_list);
            var post_url = $('#form-jobdesk').attr("action");
            var request_method = $('#form-jobdesk').attr("method");
            var post_data = new FormData($('#form-jobdesk')[0]);
            post_data.append('ticket_attachment', JSON.stringify(ticket_attachment_list));
            post_data.append('ticket_attachment_removed', JSON.stringify(ticket_attachment_list_removed));

            $.ajax({
                url: post_url,
                type: request_method,
                data: post_data,
                processData: false,
                contentType: false,
                dataType: "JSON",
                beforeSend: function () {
                    block_page('Data sedang di proses . . ');
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();
                    if (data.response.code == 200) {
                        // eval(data.response.redirect);
                        if (data.response.redirect != '') {
                            window.location.href = data.response.redirect;
                        } else {
                            window.location.reload();
                        }
                    } else {
                        notif('error', 'Kesalahan', data.response.message);
                    }
                },
                error: function (data) {
                    $.unblockUI();
                    notif('error', data.statusText, data.responseJSON.response.message);
                },
                complete: function () {

                }
            });
        }
    });
});

function removeAttachment(path, idx) {
    // console.log(path);
    // console.log(idx);
    // return 'remove attachment';
    Swal.fire({
        title: "Hapus lampiran",
        text: "Yakin hapus lampiran ini ?",
        icon: "question",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        cancelButtonText: "Tutup",
        confirmButtonClass: "btn btn-sm btn-info",
        cancelButtonClass: "btn btn-sm btn-danger m-l-20",
        buttonsStyling: false,
    }).then((result) => {
        if (result.value) {
            ticket_attachment_list_removed.push(path);
            $('.ticket-attachment-' + idx).html('');
        } else if (result.dismiss === swal.DismissReason.cancel) {
            Swal.fire({
                title: "Tutup !",
                text: "Proses dibatalkan !",
                confirmButtonClass: "btn btn-danger btn-sm",
                icon: "error",
                buttonsStyling: false,
            });
        }
    });
}
