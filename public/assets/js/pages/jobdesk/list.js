"use strict";
var table;
var KTDatatablesAdvancedColumnRendering = function () {

    var init = function () {

        table = $('#kt_datatable').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Masukan pencarian...',
                lengthMenu: '<span>Tampilkan:</span> _MENU_',
                paginate: { 'first': 'Pertama', 'last': 'Terakhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
                processing: '<i class="fas fa-spinner text-danger fa-spin fa-1x fa-fw"></i> Sedang mengambil data..'
            },
            serverSide: true,
            ajax: {
                url: $('#url-base').val() + '/jobdesk/data',
                dataType: "json",
                type: "GET",
            },
            "columnDefs": [
                { "width": "5%", "targets": 0 },
                { "width": "50%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "15%", "targets": 3 },
                { "width": "10%", "targets": 4 },
                { "width": "5%", "targets": 5 },
            ],
            columns: [
                {
                    title: '*',
                    data: 'jobact_id',
                    name: 'jobact_id',
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    sClass: 'text-center',
                },
                {
                    title: 'Title',
                    data: 'jobact_title',
                    name: 'jobact_title',
                },
                {
                    title: 'Register On',
                    data: 'jobact_reg_at',
                    name: 'jobact_reg_at',
                },
                {
                    title: 'Est. Finish',
                    data: 'jobact_finish_at',
                    name: 'jobact_finish_at',
                    'render': function (data, type, row, meta) {
                        return '<span style="width: 158px;"><div class="font-size-md text-success mb-0">' + data + '</div><div class="text-muted">' + (moment() >= moment(data).endOf('day') ? 'Over Due' : moment().to(moment(data).endOf('day'))) + '</div></span>';
                    },
                },
                {
                    title: 'Status',
                    data: 'status.status_name',
                    name: 'status.status_name',
                },
                {
                    title: 'Diperbarui',
                    data: 'created_by',
                    name: 'created_by',
                    'render': function (data, type, row, meta) {
                        return '<span style="width: 158px;"><div class="font-size-md text-success mb-0">' + row.created_at + '</div><div class="text-muted">' + (data == null ? '-' : data) + '</div></span>';
                    },
                },
                {
                    title: 'Aksi',
                    data: 'action',
                    name: 'action',
                    sClass: 'text-center'
                }
            ],
            order: [
                [1, 'asc']
            ]
        });

        $('#kt_datatable_search_status').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {

        //main function to initiate the module
        init: function () {
            init();
        }
    };
}();

jQuery(document).ready(function () {
    KTDatatablesAdvancedColumnRendering.init();
    console.log(moment().to(moment('2022-05-10', 'YYYY-MM-DD')));
    console.log(moment().to(moment('2022-04-23').endOf('day')));

});
