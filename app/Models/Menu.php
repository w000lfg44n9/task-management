<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Menu extends Authenticatable
{
    use Notifiable;

    protected $table = 'm_menu';
    protected $primaryKey = 'menu_id';
    protected $guard = 'web';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'menu_id',
        'menu_name',
        'menu_desc',
        'menu_url',
        'menu_icon',
        'menu_parent_id',
        'menu_order',
        'status_id',
        'menu_slug',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }

    public function menuParent()
    {
        return $this->hasOne(\App\Models\Menu::class, 'menu_id', 'menu_parent_id');
    }
}
