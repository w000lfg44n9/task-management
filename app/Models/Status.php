<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'm_status';
    protected $primarykey = 'status_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'status_id',
        'status_name',
        'status_description',
        'status_group',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
}
