<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'm_file';
    protected $primarykey = 'file_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'file_id',
        'file_name',
        'file_type',
        'file_extention',
        'file_size',
        'file_path',
        'foreign_id',
        'file_source',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
}
