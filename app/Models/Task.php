<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'm_task';
    protected $primarykey = 'task_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'task_id',
        'task_title',
        'task_desc',
        'task_progress',
        'task_due_date',
        'task_start_date',
        'task_assignee_to',
        'output_id',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function output()
    {
        return $this->hasOne(\App\Models\Output::class, 'output_id', 'output_id');
    }

    public function assignee()
    {
        return $this->hasOne(\App\Models\User::class,  'user_id', 'task_assignee_to');
    }

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }
}
