<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    protected $table = 'm_output';
    protected $primarykey = 'output_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'output_id',
        'output_title',
        'jobact_id',
        'department_id',
        'output_desc',
        'assignee_to',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }

    public function jobact()
    {
        return $this->hasOne(\App\Models\JobAct::class, 'jobact_id', 'jobact_id');
    }

    public function department()
    {
        return $this->hasOne(\App\Models\Department::class, 'department_id', 'department_id');
    }
}
