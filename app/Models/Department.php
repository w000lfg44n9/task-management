<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'm_department';
    protected $primarykey = 'department_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'department_id',
        'department_name',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }
}
