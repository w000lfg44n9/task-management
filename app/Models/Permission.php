<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Permission extends Authenticatable
{
    use Notifiable;

    protected $table = 'm_permission';
    protected $primaryKey = 'permission_id';
    protected $guard = 'web';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'permission_id',
        'menu_id',
        'role_id',
        'is_show',
        'is_add',
        'is_edit',
        'is_delete',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function role()
    {
        return $this->hasOne(\App\Models\Role::class, 'role_id', 'role_id');
    }

    public function menu()
    {
        return $this->hasOne(\App\Models\Menu::class, 'menu_id', 'menu_id');
    }

    public function isShow()
    {
        return $this->hasOne(\App\Models\Param::class, 'param_id', 'is_show');
    }

    public function isAdd()
    {
        return $this->hasOne(\App\Models\Param::class, 'param_id', 'is_add');
    }
    public function isEdit()
    {
        return $this->hasOne(\App\Models\Param::class, 'param_id', 'is_edit');
    }

    public function isDelete()
    {
        return $this->hasOne(\App\Models\Param::class, 'param_id', 'is_delete');
    }
}
