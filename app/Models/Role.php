<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'm_role';
    protected $primaryKey = 'role_id';
    protected $guard = 'web';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'role_name',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }

}
