<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'm_user';
    protected $primaryKey = 'user_id';
    protected $guard = 'web';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'user_first_name',
        'user_last_name',
        'user_first_title',
        'user_last_title',
        'user_email',
        'user_phone',
        'user_place_birth',
        'user_date_birth',
        'password',
        'user_avatar',
        'department_id',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function getAuthPassword() {
        return $this->password;
    }

    public function department()
    {
        return $this->hasOne(\App\Models\Department::class, 'department_id', 'department_id');
    }

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }

    protected $hidden = [
        'password'
    ];
}
