<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobAct extends Model
{
    protected $table = 'm_jobact';
    protected $primarykey = 'jobact_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'jobact_id',
        'jobact_title',
        'jobact_reg_at',
        'jobact_finish_at',
        'jobact_note',
        'status_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function status()
    {
        return $this->hasOne(\App\Models\Status::class, 'status_id', 'status_id');
    }

    public function file()
    {
        return $this->hasMany(\App\Models\File::class, 'foreign_id', 'jobact_id');
    }

    public function output()
    {
        return $this->hasMany(\App\Models\Output::class, 'jobact_id', 'jobact_id');
    }
}
