<?php

namespace App\Helpers;

use App\Models\Profile;
use App\Models\UserLogin;
use App\Models\Employee;
use App\Models\Parameter;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cookie;
use LasseRafn\Initials\Initials;
use Carbon\Carbon;
use App\Repositories\Interfaces\MenuRepository;

use finfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Helper {

    public static function generate_css($_CSS = NULL)
    {
        if(!isset($_CSS) or $_CSS == NULL) {
            return NULL;
        }
        if(is_array($_CSS)) {
            $str = "";
            foreach ($_CSS as $key => $value) {
                $str .= '<link href="'.asset($value).'" type="text/css" rel="stylesheet">';
            }

            return $str;
        } else if(is_string($_CSS)) {
            return '<link href="'.asset($_CSS).'" type="text/css" rel="stylesheet">';
        }
    }

    public static function generate_js($JS =  NULL)
    {
        if(!isset($JS) or $JS == NULL) {
            return NULL;
        }
        if(is_array($JS)) {
            $str = "";
            foreach ($JS as $key => $value) {
                $str .= '<script src="'.asset($value).'" type="text/javascript"></script>';
            }
            return $str;
        } else if(is_string($JS)) {
            return '<script src="'.asset($JS).'" type="text/javascript"></script>';
        }
    }

    public static function getActiveUser()
    {
        return Auth::guard('web')->check() ? User::with(['department'])->where('user_id', Auth::user()->user_id)->first() : false;
    }

    // public static function getEmployeeProfile()
    // {
    //     return Auth::guard('employee')->check() ? Employee::select('userid', 'username', 'nama', 'kantorid')->with(['office' => function($query) {
    //         $query->select('id', 'unitkerja');
    //     }])
    //     ->where('username', Auth::guard('employee')->user()->username)->first() : false;
    // }

    // public static function getProfileActive()
    // {
    //     return Profile::first();
    // }

	public static function checkImageAvatar($path)
    {
        // return (file_exists(asset($path))) ?  asset($path) :  asset('assets/images/no-image-available.png');
        return (File::exists($path)) ?  asset($path) :  asset('assets/images/logo-image-64x.png');
        // return (file_exists($path)) ?  asset($path) :  asset('assets/images/no-image-available.png');
    }

    public static function checkImageCategory($path)
    {
        return (file_exists($path)) ?  asset($path) :  asset('assets/user/images/category/cat1.png');
    }

    public static function checkImageUser($path)
    {
        // return (file_exists(asset($path))) ?  asset($path) :  asset('assets/images/no-image-available.png');
        return (File::exists($path)) ?  asset($path) :  asset('assets/images/no-image-available.png');
        // return (file_exists($path)) ?  asset($path) :  asset('assets/images/no-image-available.png');
    }

    public static function tgl_indo($tgl)
	{
		$tanggal = substr($tgl,8,2);
		$bulan = Helper::get_bulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
	}

	public static function tgl_indo2($tgl)
	{
		$tanggal = substr($tgl,8,2);
		$bulan = Helper::get_bulan2(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
	}

	public static function get_bulan($bln)
	{
		switch ($bln) {
				case 1 :
				return "Januari";
				break;
				case 2 :
				return "Februari";
				break;
				case 3 :
				return "Maret";
				break;
				case 4 :
				return "April";
				break;
				case 5 :
				return "Mei";
				break;
				case 6 :
				return "Juni";
				break;
				case 7 :
				return "Juli";
				break;
				case 8 :
				return "Agustus";
				break;
				case 9 :
				return "September";
				break;
				case 10 :
				return "Oktober";
				break;
				case 11 :
				return "November";
				break;
				case 12 :
				return "Desember";
				break;
			}
	}

	public static function get_bulan2($bln)
	{
		switch ($bln) {
				case 1 :
				return "Jan";
				break;
				case 2 :
				return "Feb";
				break;
				case 3 :
				return "Mar";
				break;
				case 4 :
				return "Apr";
				break;
				case 5 :
				return "Mei";
				break;
				case 6 :
				return "Jun";
				break;
				case 7 :
				return "Jul";
				break;
				case 8 :
				return "Agus";
				break;
				case 9 :
				return "Sept";
				break;
				case 10 :
				return "Okt";
				break;
				case 11 :
				return "Nov";
				break;
				case 12 :
				return "Des";
				break;
			}
	}

	public static function nameday($tgl)
	{
		$tgls=explode("-", $tgl);
		$namahari= date("l", mktime(0, 0, 0, $tgls[1], $tgls[2], $tgls[0]));
		switch ($namahari)
				{
				case "Monday" :
				return "Senin";
				break;
				case "Tuesday" :
				return "Selasa";
				break;
				case "Wednesday" :
				return "Rabu";
				break;
				case "Thursday" :
				return "Kamis";
				break;
				case "Friday" :
				return "Jumat";
				break;
				case "Saturday" :
				return "Sabtu";
				break;
				case "Sunday" :
				return "Minggu";
				break;
				}
	}

	public static function rp($int)
	{
		return "Rp, ".number_format($int, 0, '', '.');
	}

	public static function rp_nologo($int)
	{
		return number_format($int, 0, '', '.');
    }

    public static function checkDocumentType($path)
    {
        if (file_exists($path)) {
            $get_extension = pathinfo($path)['extension'];
            if (in_array($get_extension, array('png','jpg','jpeg','bmp','gif'))) {
                $attachment = '<a download="document_.'.$get_extension.'" href="'.asset($path).'" title="Document"><img src="'.asset($path).'" class="img img-responsive img-thumbnail mg-r-5-f" alt="Document" style="width:48px;height:48px;"></a>';
            }else{
                $attachment = '<a download="document_'.$get_extension.'" href="'.asset($path).'" title="Lampiran"><i class="icon-file-download2"></i></a>';
            }
            return $attachment;
        }
    }

    public static function getTreeMenu()
    {
        $menu = \App::make('App\Repositories\Interfaces\MenuRepository');
        return $menu->getTreeMenu();
    }

    public static function fetchTags($tags)
    {
        $tags = explode(';', $tags);
        $tagging = '';
        if (!empty($tags)) {
            $tagging .='<ul class="list-unstyled d-flex flex-wrap mb-40">';
            foreach ($tags as $tag) {
                $tagging .= '<li><a href="halaman?tag='.$tag.'">'.$tag.'</a></li>';
            }
            $tagging .= '</ul>';
        }
        return $tagging;
    }

    public static function createIndexing($data, $path)
    {
        $indexing = '';
        if (!empty($data)) {
            $indexing .='<ul id="myUL">';
            foreach ($data as $item) {
                $indexing .= '<li class=""><span class="caret '.(isset($_GET['tahun']) && $_GET['tahun'] == $item['year'] ? 'caret-down' : '' ).'">'.$item['year'].'</span>';
                if (!empty($item['month'])) {
                    $indexing .= '<ul class="nested '.(isset($_GET['tahun']) && $_GET['tahun'] == $item['year'] ? 'active' : '' ).'">';
                    foreach ($item['month'] as $m) {
                        $indexing .= '<li><a href="'.$path.'tahun='.$item['year'].'&bulan='.$m['mNameNum'].'">'.$m['mName'].' ('.$m['mCount'].')</a></li>';
                    }
                    $indexing .= '</ul>';
                }
                $indexing .= '</li>';
            }
            $indexing .= '</ul>';
        }
        return $indexing;
    }

    public static function checkDocumentSource($path)
    {
        if (strpos($path, 'http') !== false) {
            if (strpos($path, config('app.hcp.url')) !== FALSE) {
                $get_hcp = Helper::hcp_file_get($path);
                $path = (empty($get_hcp)) ? null : $get_hcp['data'];
            }else{
                $path = $path;
            }
        }else{
            $path = asset($path);
        }

        return $path;
    }

    public static function hcp_file_set($file_name, $path, $is_get_content = FALSE) {
      $data = $is_get_content ? file_get_contents($path) : $path;
      $finfo = new finfo(FILEINFO_MIME_TYPE);
      $type = $finfo->buffer($data);
      $auth_key = 'HCP ' . base64_encode(config('app.hcp.username')) . ":" . md5(config('app.hcp.password'));

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,config('app.hcp.url') . "$file_name");
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $type","Authorization: $auth_key"));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

      $response = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      return $httpcode;
    }

    public static function hcp_file_get($file_name) {
        $auth_key = 'HCP ' . base64_encode(config('app.hcp.username')) . ":" . md5(config('app.hcp.password'));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$file_name);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: $auth_key"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch);
        curl_close($ch);

        // return $httpcode;

        $resp = [];

        if ($httpcode['http_code'] == 200) {
            $type = $httpcode['content_type'];
            $ext = explode('.', $httpcode['url']);
            $resp = array(
                'content_type' => $httpcode['content_type'],
                'extention' => end($ext),
                'is_image' => (strpos($httpcode['content_type'], 'image') !== false) ? true : false,
                'data' => "data:$type;base64,".base64_encode($response)
            );
        }
        return $resp;
    }

    public static function get_url($file) {
      $endpoint = "$file";
      $URL = config('app.hcp.url') . $endpoint;
      return $URL;
    }

    public static function hcp_file_delete($file_name) {
      $auth_key = 'HCP ' . base64_encode(config('app.hcp.username')) . ":" . md5(config('app.hcp.password'));
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,config('app.hcp.url') . "$file_name");
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: $auth_key"));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

      $response = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      return $httpcode;
    }

    public static function get_hcp_only_path($url) {
        if (strpos($url, config('app.hcp.url')) !== false) {
            $only_path = $url;
        }else{
            $only_path = substr($url, strlen(config('app.hcp.url')));
        }
        return $only_path;
    }

    public static function getDocExtension($path)
    {
        $ext = explode('.', $path);
        return end($ext);
    }

    public static function getDocFilename($path)
    {
        $filename = explode('/', $path);
        return end($filename);
    }

    public static function generate_meta_string($string) {
        $path = str_replace(' ','-',substr(strtolower(preg_replace('/\s+/', ' ',preg_replace('/[^a-zA-Z0-9 \']/', '',$string))),0,150));
        return $path;
    }

    public static function random_symbol_color($tipe)
    {
        switch ($tipe) {
            case 'light':
                $color = array("symbol-light-success","symbol-light-primary","symbol-light-danger","symbol-light-info","symbol-light-warning","symbol-light-dark");
                break;
            case 'dark':
                $color = array("symbol-success","symbol-primary","symbol-danger","symbol-info","symbol-warning","symbol-dark");
                break;
            default:
                $color = array("symbol-success","symbol-primary","symbol-danger","symbol-info","symbol-warning","symbol-dark");
                break;
        }

        $random_keys=array_rand($color,1);
        return $color[$random_keys];
    }

    public static function random_color($tipe)
    {
        switch ($tipe) {
            case 'label':
                $color = array("label-success","label-primary","label-danger","label-info","label-warning","label-dark");
                break;
            case 'symbol':
                $color = array("symbol-success","symbol-primary","symbol-danger","symbol-info","symbol-warning","symbol-dark");
                break;
            case 'light-symbol':
                $color = array("symbol-light-success","symbol-light-primary","symbol-light-danger","symbol-light-info","symbol-light-warning","symbol-light-dark");
                break;
            case 'text':
                $color = array("text-success","text-primary","text-danger","text-info","text-warning","text-dark");
                break;
            default:
                $color = array("bg-success","bg-primary","bg-danger","bg-info","bg-warning","bg-dark");
                break;
        }

        $random_keys=array_rand($color,1);
        return $color[$random_keys];
    }

    public static function generate_initials($name)
    {

        return (new Initials)->name($name)->generate();
    }

    public static function checkTicketAttachments($path, $limit = false, $is_removed = false)
    {

        $attach = json_decode($path);
        $attachment_list = '';
        $file = '';
        // if (is_array($path)) {
            foreach ($path as $key => $item) {

                if ($limit && $key == 2) {
                    break;
                }
                $file = Helper::checkDocumentSource($item->file_path);
                $get_extension = Helper::getDocExtension($item);
                // if (in_array($get_extension, array('png','jpg','jpeg','bmp','gif'))) {
                //     $attachment = '<a download="lampiran-'.$key.'.'.$get_extension.'" href="'.$file.'" title="Document">
                //     <img src="'.$file.'" class="img img-responsive img-thumbnail mr-2" alt="lampiran '.$key.'" style="width:48px;height:48px;">
                //     </a>';
                // }else{
                    // $attachment = '<a download="lampiran-'.$key.'.'.$get_extension.'" href="'.$file.'" title="Lampiran"><i class="icon-md fas fa-file mr-2 py-1"></i> '.Helper::getDocFilename($item).'</a>';
                    $attachment = '<div class="d-flex align-items-center ticket-attachment-'.$key.'">';
                    // $attachment .= '<div class="d-flex "><a download="'.Helper::getDocFilename($item).'" href="'.$file.'" class="d-flex align-items-center text-muted text-hover-success py-1" title="'.Helper::getDocFilename($item).'"><span class="flaticon2-clip-symbol text-warning icon-1x mr-2"></span>'.Helper::getDocFilename($item).'</a></div>';
                    $attachment .= '<div class="d-flex "><a download="'.$item->file_name.'" target="_blank" href="'.$file.'" class="d-flex align-items-center text-muted text-hover-success py-1" title="'.$item->file_name.'"><span class="flaticon2-clip-symbol text-warning icon-1x mr-2"></span>'.$item->file_name.'</a></div>';

                    if ($is_removed) {
                        $attachment .= '<div class="d-flex pl-5"><a href="javascript:;" onclick="removeAttachment(`'.$item->file_id.'`, '.$key.')" class="text-hover-danger"><i class="fas fa-times-circle" title="Remove Attachment"></i></a></div>';
                    }

                    $attachment .= '</div>';
                // }

                $attachment_list .= $attachment;
            }

            if ($limit) {
                $attachment_list .= '<a href="#" class="d-flex align-items-center text-muted text-hover-primary py-1" title="...">...</a>';
            }
        // }else{
        //     $file = Helper::checkDocumentSource($path);
        //     $get_extension = Helper::getDocExtension($path);

        //     $attachment = '<a download="'.Helper::getDocFilename($path).'" href="'.$file.'" class="d-flex align-items-center text-muted text-hover-primary py-1"><span class="flaticon2-clip-symbol text-warning icon-1x mr-2"></span>'.Helper::getDocFilename($path).'</a>';

        //     $attachment_list .= $attachment;
        // }

        $attachment_list .= '';
        return $path == '' ? '' : $attachment_list;
    }



    public static function generate_sliced_text($text, $limit)
    {
        return substr(strip_tags($text), 0, $limit).(strlen(strip_tags($text)) > $limit ? '...' : '');
    }

    public static function generate_sliced_content_text($text, $limit)
    {
        return substr(preg_replace('#<[^>]+>#', ' ', $text), 0, $limit).(strlen(preg_replace('#<[^>]+>#', ' ', $text)) > $limit ? '...' : '');
    }


}

?>
