<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\AuthServices;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authServices;

    public function __construct(AuthServices $authServices)
    {
      $this->authServices = $authServices;
    }

    public function login(Request $request)
    {
        return $this->authServices->login();
    }

    public function loginSubmit(Request $request)
    {
        return $this->authServices->submitlogin($request);
    }

    public function logout()
    {
        return $this->authServices->logout();
    }
}
