<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\TaskServices;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $taskServices;

    public function __construct(TaskServices $taskServices)
    {
      $this->taskServices = $taskServices;
    }

    public function index()
    {
        return $this->taskServices->index();
    }

    public function getData(Request $request)
    {
        return $this->taskServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->taskServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->taskServices->submit($request);
    }
}
