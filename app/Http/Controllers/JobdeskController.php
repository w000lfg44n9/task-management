<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\JobdeskServices;
use Illuminate\Http\Request;

class JobdeskController extends Controller
{
    protected $jobdeskServices;

    public function __construct(JobdeskServices $jobdeskServices)
    {
      $this->jobdeskServices = $jobdeskServices;
    }

    public function index()
    {
        return $this->jobdeskServices->index();
    }

    public function getData(Request $request)
    {
        return $this->jobdeskServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->jobdeskServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->jobdeskServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->jobdeskServices->delete($request);
    }

    public function getDropdown(Request $request)
    {
        return $this->jobdeskServices->getDropdown($request);
    }
}
