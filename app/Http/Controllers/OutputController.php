<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\OutputServices;
use Illuminate\Http\Request;

class OutputController extends Controller
{
    protected $outputServices;

    public function __construct(OutputServices $outputServices)
    {
      $this->outputServices = $outputServices;
    }

    public function index()
    {
        return $this->outputServices->index();
    }

    public function getData(Request $request)
    {
        return $this->outputServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->outputServices->form($request);
    }

    public function formChange(Request $request)
    {
        return $this->outputServices->formChange($request);
    }

    public function submit(Request $request)
    {
        return $this->outputServices->submit($request);
    }

    public function update(Request $request)
    {
        return $this->outputServices->update($request);
    }

    public function delete(Request $request)
    {
        return $this->outputServices->delete($request);
    }

    public function getByJobDept(Request $request)
    {
        return $this->outputServices->getByJobDept($request);
    }

    public function getDropdown(Request $request)
    {
        return $this->outputServices->getDropdown($request);
    }
}
