<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\MenuServices;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    protected $menuServices;

    public function __construct(MenuServices $menuServices)
    {
      $this->menuServices = $menuServices;
    }

    public function index(Request $request)
    {
        return $this->menuServices->index();
    }

    public function getData(Request $request)
    {
        return $this->menuServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->menuServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->menuServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->menuServices->delete($request);
    }

    public function getDropdown(Request $request)
    {
        return $this->menuServices->getDropdown($request);
    }
}
