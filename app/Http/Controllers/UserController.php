<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\UserServices;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userServices;

    public function __construct(UserServices $userServices)
    {
      $this->userServices = $userServices;
    }

    public function index(Request $request)
    {
        return $this->userServices->index();
    }

    public function getData(Request $request)
    {
        return $this->userServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->userServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->userServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->userServices->delete($request);
    }

    public function getDropdown(Request $request)
    {
        return $this->userServices->getDropdown($request);
    }
}
