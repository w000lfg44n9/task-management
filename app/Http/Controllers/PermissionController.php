<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\PermissionServices;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    protected $permissionServices;

    public function __construct(PermissionServices $permissionServices)
    {
      $this->permissionServices = $permissionServices;
    }

    public function index(Request $request)
    {
        return $this->permissionServices->index();
    }

    public function getData(Request $request)
    {
        return $this->permissionServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->permissionServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->permissionServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->permissionServices->delete($request);
    }

    public function getDropdownMenu(Request $request)
    {
        return $this->permissionServices->getDropdownMenu($request);
    }

    public function getDropdownRole(Request $request)
    {
        return $this->permissionServices->getDropdownRole($request);
    }

    public function getDropdownShow(Request $request)
    {
        return $this->permissionServices->getDropdownShow($request);
    }

    public function getDropdownAdd(Request $request)
    {
        return $this->permissionServices->getDropdownAdd($request);
    }

    public function getDropdownEdit(Request $request)
    {
        return $this->permissionServices->getDropdownEdit($request);
    }

    public function getDropdownDelete(Request $request)
    {
        return $this->departmentServices->getDropdownDelete($request);
    }
}
