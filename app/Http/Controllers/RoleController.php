<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\RoleServices;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $roleServices;

    public function __construct(RoleServices $roleServices)
    {
      $this->roleServices = $roleServices;
    }

    public function index(Request $request)
    {
        return $this->roleServices->index();
    }

    public function getData(Request $request)
    {
        return $this->roleServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->roleServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->roleServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->roleServices->delete($request);
    }
}
