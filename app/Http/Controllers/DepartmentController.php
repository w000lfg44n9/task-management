<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\DepartmentServices;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $departmentServices;

    public function __construct(DepartmentServices $departmentServices)
    {
      $this->departmentServices = $departmentServices;
    }

    public function getDropdown(Request $request)
    {
        return $this->departmentServices->getDropdown($request);
    }

    public function index(Request $request)
    {
        return $this->departmentServices->index();
    }

    public function getData(Request $request)
    {
        return $this->departmentServices->getData($request);
    }

    public function form(Request $request)
    {
        return $this->departmentServices->form($request);
    }

    public function submit(Request $request)
    {
        return $this->departmentServices->submit($request);
    }

    public function delete(Request $request)
    {
        return $this->departmentServices->delete($request);
    }
}
