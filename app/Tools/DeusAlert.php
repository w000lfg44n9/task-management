<?php

  namespace App\Tools;
/**
 *
 */
use Illuminate\Session\Store;


class DeusAlert
{

  /**
   * Session storage.
   *
   * @var Illuminate\Session\Store
   */
  protected $session;

  public function __construct(Store $session)
  {
      $this->session = $session;
  }

  /**
     * Set a session key and value.
     *
     * @param  mixed $key
     * @param  string $data
     *
     * @return mixed
     */
    public function message($head, $message, $type = 'danger')
    {
        // if (is_array($key)) {
        //     return $this->flashMany($key);
        // }
        $this->session->flash('alert_head', $head);
        $this->session->flash('alert_message', $message);
        $this->session->flash('alert_type', $type);

    }
    /**
     * Flash multiple key/value pairs.
     *
     * @param  array  $data
     * @return void
     */
    public function flashMany(array $data)
    {
        foreach ($data as $key => $value) {
            $this->message($key, $value);
        }
    }
    /**
     * Get a value from session storage.
     *
     * @param  string $key
     * @return string
     */
    public function get($key)
    {
        return $this->session->get($key);
    }

}


 ?>
