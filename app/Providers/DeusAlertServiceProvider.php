<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tools\DeusAlert;

class DeusAlertServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      // $this->app->bind('DeusAlert', function () {
      //     return new \App\Tools\DeusAlert;
      // });

      $this->app->singleton('DeusAlert', function () {
          return $this->app->make(\App\Tools\DeusAlert::class);
      });
    }

}
