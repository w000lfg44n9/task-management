<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         // Services
         $this->app->bind('App\Services\Interfaces\AuthServices', 'App\Services\Imp\AuthServicesImp');
         $this->app->bind('App\Services\Interfaces\UserServices', 'App\Services\Imp\UserServicesImp');
         $this->app->bind('App\Services\Interfaces\DepartmentServices', 'App\Services\Imp\DepartmentServicesImp');
         $this->app->bind('App\Services\Interfaces\JobdeskServices', 'App\Services\Imp\JobdeskServicesImp');
         $this->app->bind('App\Services\Interfaces\RoleServices', 'App\Services\Imp\RoleServicesImp');
         $this->app->bind('App\Services\Interfaces\MenuServices', 'App\Services\Imp\MenuServicesImp');
         $this->app->bind('App\Services\Interfaces\PermissionServices', 'App\Services\Imp\PermissionServicesImp');
         $this->app->bind('App\Services\Interfaces\OutputServices', 'App\Services\Imp\OutputServicesImp');
         $this->app->bind('App\Services\Interfaces\TaskServices', 'App\Services\Imp\TaskServicesImp');

        // Repository
        $this->app->bind('App\Repositories\Interfaces\UserRepository', 'App\Repositories\Imp\UserRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\DepartmentRepository', 'App\Repositories\Imp\DepartmentRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\JobActRepository', 'App\Repositories\Imp\JobActRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\FileRepository', 'App\Repositories\Imp\FileRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\OutputRepository', 'App\Repositories\Imp\OutputRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\TaskRepository', 'App\Repositories\Imp\TaskRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\RoleRepository', 'App\Repositories\Imp\RoleRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\MenuRepository', 'App\Repositories\Imp\MenuRepositoryImp');
        $this->app->bind('App\Repositories\Interfaces\PermissionRepository', 'App\Repositories\Imp\PermissionRepositoryImp');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');
    }
}
