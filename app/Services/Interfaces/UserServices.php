<?php

namespace App\Services\Interfaces;

interface UserServices
{
    public function index();
    public function getData($request);
    public function form($request);
    public function submit($request);
    public function delete($request);
    public function logout();
    public function getDropdown($request);
}
