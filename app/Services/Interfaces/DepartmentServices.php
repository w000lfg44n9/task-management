<?php

namespace App\Services\Interfaces;

interface DepartmentServices
{
    public function index();
    public function getData($request);
    public function form($request);
    public function submit($request);
    public function delete($request);
    public function getDropdown($request);
}
