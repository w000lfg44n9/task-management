<?php

namespace App\Services\Interfaces;

interface TaskServices
{
    public function index();
    public function getData($request);
    public function form($request);
    // public function formChange($request);
    public function submit($request);
    // public function update($request);

    // public function delete($request);
    // // public function getDropdown($request);
    // public function getByJobDept($request);

}
