<?php

namespace App\Services\Interfaces;

interface PermissionServices
{
    public function index();
    public function getData($request);
    public function form($request);
    public function submit($request);
    public function delete($request);
    public function logout();
    public function getDropdownMenu($request);
    public function getDropdownRole($request);
    public function getDropdownShow($request);
    public function getDropdownEdit($request);
    public function getDropdownAdd($request);
    public function getDropdownDelete($request);
}
