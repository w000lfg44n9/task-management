<?php

namespace App\Services\Interfaces;

interface AuthServices
{
    public function login();
    // public function loginEmployee();
    public function submitLogin($data);
    // public function submitLoginEmployee($data);

    public function logout();
    // public function logoutEmployee();
    // public function refreshCaptcha($request);
}
