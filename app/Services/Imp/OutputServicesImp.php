<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\OutputServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\FileRepository;
use App\Repositories\Interfaces\JobActRepository;
use App\Repositories\Interfaces\OutputRepository;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class OutputServicesImp extends Controller implements OutputServices
{

    protected $adminRepo;
    protected $jobactRepository;
    protected $fileRepository;
    protected $outputRepository;

    public function __construct(
        JobActRepository $jobactRepository,
        FileRepository $fileRepository,
        OutputRepository $outputRepository
    ) {
        $this->jobactRepository = $jobactRepository;
        $this->fileRepository = $fileRepository;
        $this->outputRepository = $outputRepository;
    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Output Activity'
        );
        return view('pages.output.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->outputRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_default(`'.url('/output/form/change').'`,`'.$data->output_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Confirmation`,`Delete output activity '.$data->output_title.' ?`, `'.url('output/delete/').'`, `'.$data->output_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Delete Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'output/form/submit',
            'formTitle' => (empty($request->id)) ? 'Add Output Activity' : 'Change Output Activity',
            'master' => (empty($request->id)) ? null : $this->outputRepository->find($request->id),
            'type' => (empty($request->type)) ? null : $request->type
        );
        return view('pages.output.form', $data);
    }

    public function formChange($request)
    {
        $data = array(
            'action' => url('/output/form/change'),
            'formModalTitle' => 'Change Output Activity',
            'master' => $this->outputRepository->find($request->nilai)
        );
        return view('pages.output.form_change', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'jobact_id' => $request->jobact_id,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'department_id' => $request->department_id,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->output_list)) {
                $output = json_decode($request->output_list);
                foreach ($output as $out) {
                    if (empty($out->output_id)) {
                        $request_post['output_id'] = Uuid::uuid4()->toString();
                        $request_post['output_title'] = $out->output_title;
                        $request_post['output_desc'] = $out->output_desc;
                        $request_post['created_by'] = Auth::user()->user_id;
                        $request_post['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                        $this->outputRepository->create($request_post);
                    }else{
                        $this->outputRepository->update($out->output_id, array(
                            'output_title' => $out->output_title,
                            'output_desc' => $out->output_desc,
                            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ));
                    }
                }
            }

            DeusAlert::message('Succesfully', 'Output activity succesfully added', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Jobdesk activity succesfully added");
            DB::commit();

            if (!empty($request->ticket_attachment_removed)) {
                $attachment_removed = json_decode($request->ticket_attachment_removed);

                foreach ($attachment_removed as $removed) {
                    $file_to_remove = $this->fileRepository->find($removed);
                    $this->fileRepository->delete($removed);
                    if (File::exists($file_to_remove->file_path)) {
                        Storage::delete(str_replace('storage/uploads', 'public/uploads', $file_to_remove->file_path));
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function update($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {

            $this->outputRepository->update($request->output_id, array(
                'output_title' => $request->output_title,
                'output_desc' => $request->output_desc,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ));

            DeusAlert::message('Succesfully', 'Output activity succesfully updated', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Jobdesk activity succesfully updated");
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->outputRepository->delete($request->id);
            DeusAlert::message('Succesfully', 'Output activity succesfully deleted', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Output activity succesfully deleted");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        return $this->outputRepository->getDropdown($request);
    }

    public function getByJobDept($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Successfully get output list");
            $response->setData($this->outputRepository->getByJobDept($request->jobact_id, $request->department_id));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }



}
