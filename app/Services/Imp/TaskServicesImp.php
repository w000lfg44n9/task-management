<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\TaskServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\FileRepository;
use App\Repositories\Interfaces\JobActRepository;
use App\Repositories\Interfaces\OutputRepository;
use App\Repositories\Interfaces\TaskRepository;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class TaskServicesImp extends Controller implements TaskServices
{

    protected $adminRepo;
    protected $jobactRepository;
    protected $fileRepository;
    protected $outputRepository;
    protected $taskRepository;

    public function __construct(
        JobActRepository $jobactRepository,
        FileRepository $fileRepository,
        OutputRepository $outputRepository,
        TaskRepository $taskRepository
    ) {
        $this->jobactRepository = $jobactRepository;
        $this->fileRepository = $fileRepository;
        $this->outputRepository = $outputRepository;
        $this->taskRepository = $taskRepository;
    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Task'
        );
        return view('pages.task.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->taskRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;"  class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Confirmation`,`Delete task '.$data->task_title.' ?`, `'.url('task/delete/').'`, `'.$data->task_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Delete Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'task/form/submit',
            'formTitle' => (empty($request->id)) ? 'Task Activity' : 'Task Activity',
            'master' => (empty($request->id)) ? null : $this->taskRepository->find($request->id),
            'type' => (empty($request->type)) ? null : $request->type
        );
        return view('pages.task.form', $data);
    }

    public function formChange($request)
    {
        $data = array(
            'action' => url('/output/form/change'),
            'formModalTitle' => 'Change Output Activity',
            'master' => $this->outputRepository->find($request->nilai)
        );
        return view('pages.output.form_change', $data);
    }

    public function submit($request)
    {
        dd($request->all());
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'task_id' => empty($request->task_id) ? Uuid::uuid4()->toString() : $request->task_id,
                'status_id' => empty($request->status_id) ?  '5e06af38-bd12-11ec-9076-49d0369e787e' : (empty($request->task_assignee_to) ? '79931e98-c3d0-11ec-9be0-93045ab78987' : $request->status_id) ,
                'output_id' => $request->output_id,
                'task_title' => $request->task_title,
                'task_start_date' => $request->task_start_date,
                'task_due_date' => $request->task_due_date,
                'task_desc' => $request->task_desc,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->ticket_attachment)) {
                $attachment = json_decode($request->ticket_attachment);
                foreach ($attachment as $doc) {
                    preg_match('~data:([^{]*);base64~i', $doc->filetype, $match);
                    $imgdata = base64_decode($doc->base64);
                    $file_ext = explode('.', $doc->filename);
                    Storage::put('public/uploads/task/attachment/'.$request_post['task_id'].'-'.$doc->filename, $imgdata);
                    $this->fileRepository->create(array(
                        'file_id' => Uuid::uuid4()->toString(),
                        'file_name' => $doc->filename,
                        'file_extention' => end($file_ext),
                        'file_type' => end($match),
                        'file_path' => 'storage/uploads/jobact/attachment/'.$request_post['task_id'].'-'.$doc->filename,
                        'foreign_id' => $request_post['task_id'],
                        'file_source' => 'Task',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->user_id
                    ));
                }
            }

            if (!empty($request->task_assignee_to)) {
                $assignto = json_decode($request->task_assignee_to);
                $assignto = array_reduce($assignto, function($accumulator, $item){
                    $accumulator[] = $item->code;
                    return $accumulator;
                  }, []);
                // $ticket_assigned = (empty($ticket->ticket_assigned)) ? $assignto : array_merge(json_decode($ticket->ticket_assigned), $assignto);
                // $ticket_pic = (empty($assignto)) ? $ticket->ticket_pic : $assignto[0];
                $task_pic =  $assignto[0];
                $task_assignee = $assignto;
                $request_post['task_assignee_to'] = json_encode($task_assignee);

                // $employee_interact_with = $ticket_assigned;
                // if (!empty($ticket->ticket_pic) && $ticket->ticket_pic !== null)
                //     array_push($employee_interact_with, $ticket_pic);
            }else{
                // $employee_interact_with = (empty($ticket->ticket_assigned)) ? array() : json_decode($ticket->ticket_assigned);
                // if (!empty($ticket->ticket_pic) && $ticket->ticket_pic !== null)
                //     array_push($employee_interact_with, $ticket->ticket_pic);
            }

            if (!empty($request->task_id)) {
                $sts_berita = 'updated';
                $this->taskRepository->update($request->task_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                $this->taskRepository->create($request_post);
                $sts_berita = 'added';
            }

            DeusAlert::message('Succesfully', 'Task succesfully '.$sts_berita, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Task succesfully ".$sts_berita);
            DB::commit();

            if (!empty($request->ticket_attachment_removed)) {
                $attachment_removed = json_decode($request->ticket_attachment_removed);

                foreach ($attachment_removed as $removed) {
                    $file_to_remove = $this->fileRepository->find($removed);
                    $this->fileRepository->delete($removed);
                    if (File::exists($file_to_remove->file_path)) {
                        Storage::delete(str_replace('storage/uploads', 'public/uploads', $file_to_remove->file_path));
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function update($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {

            $this->outputRepository->update($request->output_id, array(
                'output_title' => $request->output_title,
                'output_desc' => $request->output_desc,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ));

            DeusAlert::message('Succesfully', 'Output activity succesfully updated', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Jobdesk activity succesfully updated");
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->outputRepository->delete($request->id);
            DeusAlert::message('Succesfully', 'Output activity succesfully deleted', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Output activity succesfully deleted");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        return $this->jobactRepository->getDropdown($request);
    }

    public function getByJobDept($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Successfully get output list");
            $response->setData($this->outputRepository->getByJobDept($request->jobact_id, $request->department_id));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }



}
