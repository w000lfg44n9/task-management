<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;
use App\Services\Interfaces\AuthServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmployeeRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AuthServicesImp extends Controller implements AuthServices
{

    protected $userRepository;
    protected $adminLoginRepo;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function login()
    {
        $data['data'] = null;
        return view('auth.login', $data);
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('web')->logout();
        return redirect('auth/login');
    }

    public function submitlogin($request)
    {
        $response = new Response;
        $data = $request->all();
        $rules = [
            'username' => 'required',
            'password' => 'min:3|required',
        ];
        DB::beginTransaction();
        try {

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                throw new \Exception($this->validationErrorsToString($validator->errors()));
            }

            $cek_user = $this->userRepository->findByEmail($data['username']);
            if (!$cek_user) {
                throw new \Exception("Invalid Username");
            }
            if ($cek_user->status_id != '5e06af38-bd12-11ec-9076-49d0369e787e') {
                throw new \Exception("Data pengguna yang anda masukan tidak aktif. Silahkan hubungi administrator untuk melakukan aktivasi kembali");
            }
            if(Auth::guard('web')->attempt([
                'user_email' => $data['username'],
                'password' => $data['password'],
            ])){
                DeusAlert::message('Berhasil', 'Selamat datang di halaman helpdesk Badan Pendapatan Daerah Provinsi DKI Jakarta', 'success');
                $response->setStatus(true);
                $response->setCode(200);
                $response->setData(null);
                $response->setRedirect(url('dashboard'));
                $response->setMessage("Login success");
            }else{
                throw new \Exception('Kata sandi yang anda masukan salah');
            }

            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Login success");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function refreshCaptcha($request)
    {
        return response()->json([
            // 'captcha' => Captcha::img()
        ]);

    }

    public function loginEmployee()
    {
        $data['data'] = null;
        return view('pages.auth.login_employee', $data);
    }

    public function submitloginEmployee($request)
    {
        $response = new Response;
        $data = $request->all();
        $rules = [
            'username' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'recaptcha',
        ];
        DB::beginTransaction();
        try {

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                throw new \Exception($this->validationErrorsToString($validator->errors()));
            }

            $cek_user = $this->employeeRepository->findByUsername($data['username']);
            // return json_encode(array(
            //     'username' => $data['username'],
            //     'password_real' => $cek_user->password,
            //     'password_input' => $data['password'],
            //     'password' => crypt($data['password'], $cek_user->password)
            // ));
            if (!$cek_user) {
                throw new \Exception("Invalid Username");
            }

            if ($cek_user->aktif != '1') {
                throw new \Exception("Data pengguna yang anda masukan tidak aktif. Silahkan hubungi administrator untuk melakukan aktivasi kembali");
            }
            $data_employee = $this->employeeRepository->findWhere([
                ['username', '=', $data['username']],
                ['password', '=', crypt($data['password'], $cek_user->password)]
            ]);
            // return json_encode($data_employee);

            // if(Auth::guard('employee')->attempt([
            //     'username' => $data['username'],
            //     'password' => crypt($data['password'], $cek_user->password),
            // ])){
            if ($data_employee) {
                Auth::guard('employee')->login($data_employee);


                DeusAlert::message('Berhasil', 'Selamat datang di halaman admin konsol helpdesk Badan Pendapatan Daerah Provinsi DKI Jakarta', 'success');
                $response->setStatus(true);
                $response->setCode(200);
                $response->setData(null);
                $response->setRedirect(url('/'));
                $response->setMessage("Login success");
            }else{
                throw new \Exception('Kata sandi yang anda masukan salah');
            }

            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Login success");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function logoutEmployee()
    {
        if ($isSso = $this->logoutSso()){
            return $isSso;
        }
        Session::flush();
        Auth::guard('employee')->logout();
        return redirect('auth/login');
    }

    public function logoutSso(){
        $isSso = session('is_sso', false) === true && session('sso_token', null) !== null;
        if ($isSso){
            Session::flush();
            Auth::guard('employee')->logout();
            return redirect(config('services.sso_server.url'));
        }

        return false;
    }

}
