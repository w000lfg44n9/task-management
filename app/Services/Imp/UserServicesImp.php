<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\UserServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserServicesImp extends Controller implements UserServices
{

    protected $adminRepo;
    protected $userRepository;



    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;

    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List User'
        );
        return view('pages.user.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->userRepository->getListExcludeLoggenIn(Auth::user()->user_id);
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_user(`'.url('/user/form').'`,`'.$data->user_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Konfirmasi`,`Delete user '.$data->user_first_name.' ?`, `'.url('user/delete').'`, `'.$data->user_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Remove Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'user/form/submit',
            'formModalTitle' => (empty($request->nilai)) ? 'Add User' : 'Change User',
            'master' => (empty($request->nilai)) ? null : $this->userRepository->find($request->nilai)
        );

        return view('pages.user.form', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'user_id' => empty($request->user_id) ? Uuid::uuid4()->toString() : $request->user_id,
                'user_email' => $request->user_email,
                'user_first_name' => $request->user_first_name,
                'user_last_name' => $request->user_last_name,
                'user_first_title' => $request->user_first_title,
                'user_last_title' => $request->user_last_title,
                'user_phone' => $request->user_phone,
                'user_place_birth' => $request->user_place_birth,
                'user_date_birth' => $request->user_date_birth,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'department_id' => $request->department_id,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->password)) {
                $request_post['password'] = Hash::make($request->password);
            }

            if ($request->hasFile('profile_avatar')) {
                Storage::putFileAs('public/uploads/image/avatar', $request->file('profile_avatar'), $request_post['user_id'].'.'.$request->file('profile_avatar')->extension());
                $request_post['user_avatar'] = 'storage/uploads/image/avatar/'.$request_post['user_id'].'.'.$request->file('profile_avatar')->extension();
            }

            if (!empty($request->user_id)) {
                $action_status = 'Diperbarui';
                $this->userRepository->update($request->user_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d');
                $this->userRepository->create($request_post);
                $action_status = 'ditambahkan';
            }

            DeusAlert::message('Berhasil', 'Data pengguna berhasil '.$action_status, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data pengguna berhasil ".$action_status);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->userRepository->delete($request->id);
            DeusAlert::message('Berhasil', 'Data pengguna berhasil dihapus', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data pengguna berhasil dihapus");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        return $this->userRepository->getDropdown($request);
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('web')->logout();
        return redirect('auth/login');
    }


}
