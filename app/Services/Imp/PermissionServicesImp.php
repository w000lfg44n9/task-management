<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\PermissionRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\PermissionServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PermissionServicesImp extends Controller implements PermissionServices
{

    protected $adminRepo;
    protected $permissionRepository;



    public function __construct(
        PermissionRepository $permissionRepository
    ) {
        $this->permissionRepository = $permissionRepository;

    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Permission'
        );
        return view('pages.permission.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->permissionRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_user(`'.url('/permission_id/form').'`,`'.$data->permission_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Konfirmasi`,`Delete permission ?`, `'.url('permission/delete').'`, `'.$data->permission_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Remove Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'permission/form/submit',
            'formModalTitle' => (empty($request->nilai)) ? 'Add permission' : 'Change permission',
            'master' => (empty($request->nilai)) ? null : $this->permissionRepository->find($request->nilai)
        );

        return view('pages.permission.form', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'permission_id' => empty($request->permission_id) ? Uuid::uuid4()->toString() : $request->permission_id,
                'menu_id' => $request->menu_id,
                'role_id' => $request->role_id,
                'is_show' => $request->is_show,
                'is_add' => $request->is_add,
                'is_edit' => $request->is_edit,
                'is_delete' => $request->is_delete,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->permission_id)) {
                $action_status = 'Diperbarui';
                $this->permissionRepository->update($request->permission_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d');
                $this->permissionRepository->create($request_post);
                $action_status = 'ditambahkan';
            }

            DeusAlert::message('Berhasil', 'Data permission berhasil '.$action_status, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data permission berhasil ".$action_status);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->permissionRepository->delete($request->id);
            DeusAlert::message('Berhasil', 'Data permission berhasil dihapus', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data permission berhasil dihapus");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('web')->logout();
        return redirect('auth/login');
    }

    public function getDropdownMenu($request) {
        return $this->permissionRepository->getDropdownMenu($request);
    }

    public function getDropdownRole($request) {
        return $this->permissionRepository->getDropdownRole($request);
    }
    public function getDropdownShow($request) {
        return $this->permissionRepository->getDropdownShow($request);
    }
    public function getDropdownAdd($request) {
        return $this->permissionRepository->getDropdownAdd($request);
    }
    public function getDropdownEdit($request) {
        return $this->permissionRepository->getDropdownEdit($request);
    }
    public function getDropdownDelete($request) {
        return $this->permissionRepository->getDropdownDelete($request);
    }


}
