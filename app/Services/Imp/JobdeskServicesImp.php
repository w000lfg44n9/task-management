<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\JobdeskServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\FileRepository;
use App\Repositories\Interfaces\JobActRepository;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class JobdeskServicesImp extends Controller implements JobdeskServices
{

    protected $adminRepo;
    protected $jobactRepository;
    protected $fileRepository;


    public function __construct(
        JobActRepository $jobactRepository,
        FileRepository $fileRepository
    ) {
        $this->jobactRepository = $jobactRepository;
        $this->fileRepository = $fileRepository;
    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Jobdesk Activity'
        );
        return view('pages.jobdesk.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->jobactRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="'.url('jobdesk/form/'.$data->jobact_id.'/change').'" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Confirmation`,`Delete jobdesk '.$data->jobact_title.' ?`, `'.url('jobdesk/delete/').'`, `'.$data->jobact_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Delete Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'jobdesk/form/submit',
            'formTitle' => (empty($request->id)) ? 'Add Jobdesk Activity' : 'Change Jobdesk Activity',
            'master' => (empty($request->id)) ? null : $this->jobactRepository->find($request->id),
            'type' => (empty($request->type)) ? null : $request->type
        );
        return view('pages.jobdesk.form', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'jobact_id' => empty($request->jobact_id) ? Uuid::uuid4()->toString() : $request->jobact_id,
                'jobact_title' => $request->jobact_title,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'jobact_reg_at' => $request->jobact_reg_at,
                'jobact_finish_at' => $request->jobact_finish_at,
                'jobact_note' => $request->jobact_note,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->ticket_attachment)) {
                $attachment = json_decode($request->ticket_attachment);
                foreach ($attachment as $doc) {
                    preg_match('~data:([^{]*);base64~i', $doc->filetype, $match);
                    $imgdata = base64_decode($doc->base64);
                    $file_ext = explode('.', $doc->filename);
                    Storage::put('public/uploads/jobact/attachment/'.$request_post['jobact_id'].'-'.$doc->filename, $imgdata);
                    $this->fileRepository->create(array(
                        'file_id' => Uuid::uuid4()->toString(),
                        'file_name' => $doc->filename,
                        'file_extention' => end($file_ext),
                        'file_type' => end($match),
                        'file_path' => 'storage/uploads/jobact/attachment/'.$request_post['jobact_id'].'-'.$doc->filename,
                        'foreign_id' => $request_post['jobact_id'],
                        'file_source' => 'Jobdesk Activity',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->user_id
                    ));
                }
            }

            if (!empty($request->jobact_id)) {
                $sts_berita = 'updated';
                $this->jobactRepository->update($request->jobact_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                $this->jobactRepository->create($request_post);
                $sts_berita = 'added';
            }

            DeusAlert::message('Succesfully', 'Jobdesk activity succesfully '.$sts_berita, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Jobdesk activity succesfully ".$sts_berita);
            DB::commit();

            if (!empty($request->ticket_attachment_removed)) {
                $attachment_removed = json_decode($request->ticket_attachment_removed);

                foreach ($attachment_removed as $removed) {
                    $file_to_remove = $this->fileRepository->find($removed);
                    $this->fileRepository->delete($removed);
                    if (File::exists($file_to_remove->file_path)) {
                        Storage::delete(str_replace('storage/uploads', 'public/uploads', $file_to_remove->file_path));
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->jobactRepository->delete($request->id);
            $list_file = $this->fileRepository->findByForeign('Jobdesk Activity', $request->id);
            foreach ($list_file as $file) {
                $this->fileRepository->delete($file->file_id);
                if (File::exists($file->file_path)) {
                    Storage::delete(str_replace('storage/uploads', 'public/uploads', $file->file_path));
                }
            }
            DeusAlert::message('Succesfully', 'Jobdesk activity succesfully deleted', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Jobdesk activity succesfully deleted");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        return $this->jobactRepository->getDropdown($request);
    }


}
