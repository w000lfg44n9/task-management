<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\DepartmentServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\DepartmentRepository;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class DepartmentServicesImp extends Controller implements DepartmentServices
{

    protected $adminRepo;
    protected $departmentRepository;



    public function __construct(
        DepartmentRepository $departmentRepository
    ) {
        $this->departmentRepository = $departmentRepository;

    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Department'
        );
        return view('pages.department.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->departmentRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_default(`'.url('department/form').'`,`'.$data->department_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Confirmation`,`Delete department '.$data->departname_name.' ?`, `'.url('department/delete/').'`, `'.$data->user_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Delete Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'department/form/submit',
            'formModalTitle' => (empty($request->nilai)) ? 'Add Department' : 'Change Department',
            'master' => (empty($request->nilai)) ? null : $this->departmentRepository->find($request->nilai)
        );
        return view('pages.department.form', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'department_id' => empty($request->department_id) ? Uuid::uuid4()->toString() : $request->department_id,
                'department_name' => $request->department_name,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->department_id)) {
                $sts_berita = 'updated';
                $this->departmentRepository->update($request->department_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d');
                $this->departmentRepository->create($request_post);
                $sts_berita = 'added';
            }

            DeusAlert::message('Berhasil', 'Department succesfully '.$sts_berita, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Department succesfully ".$sts_berita);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->departmentRepository->delete($request->id);
            DeusAlert::message('Berhasil', 'Department succesfully deleted', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Department succesfully deleted");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        return $this->departmentRepository->getDropdown($request);
    }


}
