<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\UserRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\RoleServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\RoleRepository;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RoleServicesImp extends Controller implements RoleServices
{

    protected $adminRepo;
    protected $roleRepository;



    public function __construct(
        RoleRepository $roleRepository
    ) {
        $this->roleRepository = $roleRepository;

    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Role'
        );
        return view('pages.role.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->roleRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_default(`'.url('/role/form').'`,`'.$data->role_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Konfirmasi`,`Delete user '.$data->role_name.' ?`, `'.url('user/delete').'`, `'.$data->role_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Remove Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {
        $data = array(
            'action' => config('app.url').'role/form/submit',
            'formModalTitle' => (empty($request->nilai)) ? 'Add Role' : 'Change Role',
            'master' => (empty($request->nilai)) ? null : $this->roleRepository->find($request->nilai)
        );

        return view('pages.role.form', $data);
    }

    public function submit($request)
    {
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'role_id' => empty($request->role_id) ? Uuid::uuid4()->toString() : $request->role_id,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'role_name' => $request->role_name,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );

            if (!empty($request->role_id)) {
                $action_status = 'Diperbarui';
                $this->roleRepository->update($request->role_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d');
                $this->roleRepository->create($request_post);
                $action_status = 'ditambahkan';
            }

            DeusAlert::message('Berhasil', 'Data role berhasil '.$action_status, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data role berhasil ".$action_status);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->roleRepository->delete($request->id);
            DeusAlert::message('Berhasil', 'Data role berhasil dihapus', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data role berhasil dihapus");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
        // return $this->userRepository->getDropdown($request);
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('web')->logout();
        return redirect('auth/login');
    }


}
