<?php

namespace App\Services\Imp;

use App\Facades\DeusAlert;
use App\Repositories\Interfaces\MenuRepository;

use Yajra\DataTables\Facades\DataTables;
use App\Services\Interfaces\MenuServices;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MenuServicesImp extends Controller implements MenuServices
{

    protected $adminRepo;
    protected $menuRepository;



    public function __construct(
        MenuRepository $menuRepository
    ) {
        $this->menuRepository = $menuRepository;

    }

    public function index()
    {
        $data = array(
            'formTitle' => 'List Menu'
        );
        return view('pages.menu.index', $data);
    }

    public function getData($request)
    {
        try {
            $data = $this->menuRepository->all();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Action:
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="get_modal_user(`'.url('/menu/form').'`,`'.$data->menu_id.'`, `edit`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <span class="navi-text">Change Data</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" onclick="konfirm(`Konfirmasi`,`Delete user '.$data->menu_name.' ?`, `'.url('menu/delete').'`, `'.$data->menu_id.'`)" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="navi-text">Remove Data</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        ';
            })
            ->make(true);
        }
        catch(\Exception $e)
        {
            return  $e->getMessage();
        }
    }

    public function form($request)
    {

        $data = array(
            'action' => config('app.url').'menu/form/submit',
            'formModalTitle' => (empty($request->nilai)) ? 'Add Menu' : 'Change Menu',
            'master' => (empty($request->nilai)) ? null : $this->menuRepository->find($request->nilai)
        );

        return view('pages.menu.form', $data);
    }

    public function submit($request)
    {
        dd($request->all());
        $response = new Response;
        DB::beginTransaction();
        try {
            $request_post = array(
                'menu_id' => empty($request->menu_id) ? Uuid::uuid4()->toString() : $request->menu_id,
                'menu_name' => $request->menu_name,
                'menu_desc' => $request->menu_desc,
                'menu_url' => $request->menu_url,
                'menu_icon' => $request->menu_icon,
                'menu_parent_id' => $request->menu_parent_id,
                'menu_order' => $request->menu_order,
                'status_id' => empty($request->status_id) ? '5e06af38-bd12-11ec-9076-49d0369e787e' : $request->status_id,
                'menu_slug' => $request->menu_slug,
                'updated_by' => Auth::user()->user_id,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            );


            if (!empty($request->menu_id)) {
                $action_status = 'Diperbarui';
                $this->menuRepository->update($request->menu_id, $request_post);
            }else{
                $request_post['created_by'] = Auth::user()->user_id;
                $request_post['created_at'] = Carbon::now()->format('Y-m-d');
                $this->menuRepository->create($request_post);
                $action_status = 'ditambahkan';
            }

            DeusAlert::message('Berhasil', 'Data pengguna berhasil '.$action_status, 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data pengguna berhasil ".$action_status);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function delete($request) {
        $response = new Response;
        DB::beginTransaction();
        try {
            $this->menuRepository->delete($request->id);
            DeusAlert::message('Berhasil', 'Data menu berhasil dihapus', 'success');
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage("Data menu berhasil dihapus");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }

    public function getDropdown($request) {
         return $this->menuRepository->getDropdown($request);
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('web')->logout();
        return redirect('auth/login');
    }


}
