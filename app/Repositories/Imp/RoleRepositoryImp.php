<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\RoleRepository;
use App\Models\Role;

class RoleRepositoryImp implements RoleRepository
{

    public function all()
    {
        return Role::with(['status'])->get();
    }

    public function getListExcludeLoggenIn($role)
    {
        return Role::with(['status'])->where('role_id', '!=', $role)->get();
    }

    public function create($data)
    {
        return Role::create($data);
    }

    public function find($id)
    {
        return Role::with(['status'])->where('role_id', '=', $id)->first();
    }

    public function delete($id)
    {
        return Role::destroy($id);
    }

    public function update($id, array $data)
    {
        return Role::find($id)->update($data);
    }

    public function findByEmail($id)
    {
        return Role::where('user_email', $id)->first();
    }

}
