<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\PermissionRepository;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Menu;
use App\Models\Param;

class PermissionRepositoryImp implements PermissionRepository
{

    public function all()
    {
        return Permission::with(['menu'])->with(['role'])->with(['isShow'])->with(['isAdd'])->with(['isEdit'])->with(['isDelete'])->get();
    }

    public function getListExcludeLoggenIn($permission)
    {
        return Permission::with(['status'])->where('permission_id', '!=', $permission)->get();
    }

    public function create($data)
    {
        return Permission::create($data);
    }

    public function find($id)
    {
        return Permission::with(['status'])->where('permission_id', '=', $id)->first();
    }

    public function delete($id)
    {
        return Permission::destroy($id);
    }

    public function update($id, array $data)
    {
        return Permission::find($id)->update($data);
    }

    public function getDropdownMenu($request)
    {
        $data = Menu::orderBy('role_name', 'ASC')->where('role_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('menu_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getDropdownRole($request)
    {
        $data = Role::orderBy('role_name', 'ASC')->where('role_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('role_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getDropdownShow($request)
    {
        $data = Param::orderBy('param_name', 'ASC')->where('param_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('param_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getDropdownAdd($request)
    {
        $data = Param::orderBy('param_name', 'ASC')->where('param_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('param_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getDropdownEdit($request)
    {
        $data = Param::orderBy('param_name', 'ASC')->where('param_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('param_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getDropdownDelete($request)
    {
        $data = Param::orderBy('param_name', 'ASC')->where('param_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('param_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

}
