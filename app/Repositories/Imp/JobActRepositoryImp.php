<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\JobActRepository;
use App\Models\JobAct;

class JobActRepositoryImp implements JobActRepository
{

    public function all()
    {
        return JobAct::with(['status', 'file'])->get();
    }

    public function create($data)
    {
        return JobAct::create($data);
    }

    public function find($id)
    {
        return JobAct::with(['status', 'file'])->where('jobact_id', $id)->first();
    }

    public function delete($id)
    {
        return JobAct::where('jobact_id', $id)->delete();
    }

    public function update($id, array $data)
    {
        return JobAct::where('jobact_id', $id)->update($data);
    }

    public function getDropdown($request)
    {
        $data = JobAct::orderBy('jobact_title', 'ASC');
        if (!empty($request->search)) {
            $data->where('jobact_title', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

}
