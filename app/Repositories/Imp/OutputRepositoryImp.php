<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\OutputRepository;
use App\Models\Output;

class OutputRepositoryImp implements OutputRepository
{

    public function all()
    {
        return Output::with(['status', 'jobact', 'department'])->get();
    }

    public function create($data)
    {
        return Output::create($data);
    }

    public function find($id)
    {
        return Output::with(['status', 'jobact', 'department'])->where('output_id', $id)->first();
    }

    public function delete($id)
    {
        return Output::where('output_id', $id)->delete();
    }

    public function update($id, array $data)
    {
        return Output::where('output_id', $id)->update($data);
    }

    public function getDropdown($request)
    {
        $data = Output::orderBy('output_title', 'ASC');

        if (!empty($request->jobact_id))
            $data->where('jobact_id', '=', $request->jobact_id);

        if (!empty($request->dept_id))
            $data->where('department_id', '=', $request->dept_id);

        if (!empty($request->search)) {
            $data->where('output_title', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getByJobDept($jobact_id, $dept_id)
    {
        return Output::where([
            ['jobact_id', '=', $jobact_id],
            ['department_id', '=', $dept_id]
        ])->get();
    }

}
