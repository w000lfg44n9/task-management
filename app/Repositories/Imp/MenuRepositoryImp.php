<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\MenuRepository;
use App\Models\Menu;

class MenuRepositoryImp implements MenuRepository
{

    public function all()
    {
        return Menu::with(['status'])->with(['menuParent'])->get();
    }

    public function getListExcludeLoggenIn($menu)
    {
        return Menu::with(['status'])->where('menu_id', '!=', $menu)->get();
    }

    public function create($data)
    {
        return Menu::create($data);
    }

    public function find($id)
    {
        return Menu::with(['status'])->with(['menuParent'])->where('menu_id', '=', $id)->first();
    }

    public function delete($id)
    {
        return Menu::destroy($id);
    }

    public function update($id, array $data)
    {
        return Menu::find($id)->update($data);
    }

    public function findByEmail($id)
    {
        return Menu::where('user_email', $id)->first();
    }

    public function getDropdown($request)
    {
        $data = Menu::orderBy('menu_name', 'ASC');
        if (!empty($request->search)) {
            $data->where('menu_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

}
