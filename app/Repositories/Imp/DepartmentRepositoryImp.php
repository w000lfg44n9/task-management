<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\DepartmentRepository;
use App\Models\Department;

class DepartmentRepositoryImp implements DepartmentRepository
{

    public function all()
    {
        return Department::with(['status'])->get();
    }

    public function create($data)
    {
        return Department::create($data);
    }

    public function find($id)
    {
        return Department::with(['status'])->where('department_id', $id)->first();
    }

    public function delete($id)
    {
        return Department::destroy($id);
    }

    public function update($id, array $data)
    {
        return Department::where('department_id', $id)->update($data);
    }

    public function getDropdown($request)
    {
        $data = Department::orderBy('department_name', 'ASC')->where('department_id', '!=', '27f56208-bd13-11ec-9076-49d0369e787e');
        if (!empty($request->search)) {
            $data->where('department_name', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

}
