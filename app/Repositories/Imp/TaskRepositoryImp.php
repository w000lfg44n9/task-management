<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\TaskRepository;
use App\Models\Task;
use Illuminate\Support\Facades\DB;

class TaskRepositoryImp implements TaskRepository
{

    public function all()
    {
        return Task::with(['status',
                    'output' => function($query) {
                        $query->select('output_id', 'output_title', 'jobact_id', 'department_id')->with([
                            'jobact' => function($query) {
                                $query->select('jobact_id','jobact_title');
                            },
                            'department' => function($query) {
                                $query->select('department_id','department_name');
                            }
                        ]);
                    },
                    // 'assignee' => function($query) {
                    //     $query->select('user_id', DB::RAW("concat(user_first_name, ' ', user_last_name) as user_name"));
                    // }
                ])
                ->get();
    }

    public function create($data)
    {
        return Task::create($data);
    }

    public function find($id)
    {
        return Task::with(['status', 'output', 'asignee'])->where('output_id', $id)->first();
    }

    public function delete($id)
    {
        return Task::where('output_id', $id)->delete();
    }

    public function update($id, array $data)
    {
        return Task::where('output_id', $id)->update($data);
    }

    public function getDropdown($request)
    {
        $data = Task::orderBy('output_title', 'ASC');
        if (!empty($request->search)) {
            $data->where('output_title', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function getByJobDept($jobact_id, $dept_id)
    {
        return Task::where([
            ['jobact_id', '=', $jobact_id],
            ['department_id', '=', $dept_id]
        ])->get();
    }

}
