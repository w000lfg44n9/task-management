<?php

namespace App\Repositories\Imp;

use App\Helpers\Helper;
use App\Repositories\Interfaces\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepositoryImp implements UserRepository
{

    public function all()
    {
        return User::all();
    }

    public function getListExcludeLoggenIn($user)
    {
        return User::with(['department','status'])->where('user_id', '!=', $user)->get();
    }

    public function create($data)
    {
        return User::create($data);
    }

    public function find($id)
    {
        return User::with(['department','status'])->where('user_id', '=', $id)->first();
    }

    public function delete($id)
    {
        return User::destroy($id);
    }

    public function update($id, array $data)
    {
        return User::find($id)->update($data);
    }

    public function findByEmail($id)
    {
        return User::where('user_email', $id)->first();
    }

    public function getDropdown($request) {
        $data = User::select('user_id', 'user_first_name', 'user_last_name', DB::RAW("concat(user_first_name, ' ', user_last_name) as user_name"),  'department_id')->with(['department' => function($query) {
            $query->select('department_id', 'department_name');
        }]);
        //minus filter by department
        if (!empty($request->search)) {
            $search = strtolower($request->search);
            $data->where(DB::RAW("lower(concat(user_first_name, ' ', user_last_name))"), 'like', "%$search%");
        }
        $data = $data->distinct()->get()->toArray();
        $data = array_reduce($data, function($accumulator, $item){
            // $nama = explode(',', $item['nama']);
            $accumulator[] = [
                'user_id' => $item['user_id'],
                'user_name' => $item['user_name'],
                'department' => $item['department'],
                'initial' => Helper::generate_initials($item['user_name'])
            ];

            return $accumulator;
          }, []);
        return $data;
    }

}
