<?php

namespace App\Repositories\Imp;

use App\Repositories\Interfaces\FileRepository;
use App\Models\File;

class FileRepositoryImp implements FileRepository
{

    public function all()
    {
        return File::all();
    }

    public function create($data)
    {
        return File::create($data);
    }

    public function find($id)
    {
        return File::where('file_id', $id)->first();
    }

    public function delete($id)
    {
        return File::where('file_id', $id)->delete();
    }

    public function update($id, array $data)
    {
        return File::where('file_id', $id)->update($data);
    }

    public function getDropdown($request)
    {
        $data = File::orderBy('jobact_title', 'ASC');
        if (!empty($request->search)) {
            $data->where('jobact_title', 'like', "%$request->search%");
        }
        return $data->distinct()->get();
    }

    public function findByForeign($foreign, $id)
    {
        return File::where([
            ['foreign_id', '=', $id],
            ['file_source', '=', $foreign]
        ])->get();
    }


}
