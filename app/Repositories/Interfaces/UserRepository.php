<?php

namespace App\Repositories\Interfaces;

interface UserRepository
{
    public function all();
    public function getListExcludeLoggenIn($user);
    public function create($data);
    public function find($id);
    public function delete($id);
    public function update($id, array $data);
    public function findByEmail($id);
    public function getDropdown($request);
}
