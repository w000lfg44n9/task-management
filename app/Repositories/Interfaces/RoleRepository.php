<?php

namespace App\Repositories\Interfaces;

interface RoleRepository
{
    public function all();
    public function getListExcludeLoggenIn($role);
    public function create($data);
    public function find($id);
    public function delete($id);
    public function update($id, array $data);
    public function findByEmail($id);
}
