<?php

namespace App\Repositories\Interfaces;

interface PermissionRepository
{
    public function all();
    public function create($data);
    public function find($id);
    public function delete($id);
    public function update($id, array $data);
   // public function getDropdownMenu($data);
    //public function getDropdownRole($data);
}
