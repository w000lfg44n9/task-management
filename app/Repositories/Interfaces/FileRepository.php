<?php

namespace App\Repositories\Interfaces;

interface FileRepository
{
    public function all();
    public function create($data);
    public function find($id);
    public function delete($id);
    public function update($id, array $data);
    public function getDropdown($data);
    public function findByForeign($foreign, $id);
}
