@extends('layouts.app')
@section('title', $formTitle)
@push('css')
{!!
    Helper::generate_css(array(
            'assets/plugins/custom/datatables/datatables.bundle.css'
        )
    )

!!}
@endpush
@section('content')
<div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">{{ $formTitle }}</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Department</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">List</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header flex-wrap py-5">
                <div class="card-title">
                    <h3 class="card-label">List Department
                    <div class="text-muted pt-2 font-size-sm">-</div></h3>
                </div>
                <div class="card-toolbar">
                    <a href="javascript:;" onclick="get_modal_default(`{{ url('department/form') }}`, null, `add`)" class="btn btn-primary font-weight-bolder">
                    Add Department</a>
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-hover table-separate table-head-custom table-checkable" id="kt_datatable">
                    <thead>
                        <tr>
                            <th>*</th>
                            <th>Department </th>
                            <th>Status</th>
                            <th>Updated</th>
                            <th>Act.</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
{!!
    Helper::generate_js(array(
            'assets/plugins/custom/datatables/datatables.bundle.js',
            'assets/js/pages/department/list.js',
        )
    )

!!}
@endpush
