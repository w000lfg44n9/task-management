<div class="modal fade" id="responsive-modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $formModalTitle }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-default-modal" method="POST" action="{{ url('role/form') }}"
                enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="role_id" id="role_id"
                        value="{{ !empty($master) ? $master->role_id : '' }}">
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Role Name <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="role_name" name="role_name"
                                placeholder="Please input role name here"
                                value="{{ !empty($master) ? $master->role_name : '' }}"
                                title="Please input role name here" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
