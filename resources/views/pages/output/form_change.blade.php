<div class="modal fade" id="responsive-modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $formModalTitle }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-default-modal" method="POST" action="{{ $action }}"
                enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="output_id" id="output_id"
                        value="{{ !empty($master) ? $master->output_id : '' }}">
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Jobdesk </label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="jobact_name" name="jobact_name"
                                placeholder="Please input department name here"
                                value="{{ !empty($master) ? $master->jobact->jobact_title : '' }}"
                                title="Please input department name here" readonly />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Department </label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="department_name" name="department_name"
                                placeholder="Please input department name here"
                                value="{{ !empty($master) ? $master->department->department_name : '' }}"
                                title="Please input department name here" readonly />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Output Title <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="output_title" name="output_title"
                                placeholder="Please input department name here"
                                value="{{ !empty($master) ? $master->output_title : '' }}"
                                title="Please input department name here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Output Desc <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control summernote" name="output_desc" id="output_desc" rows="10" title="Please input output activity description here" >{{ !empty($master) ? $master->output_desc : '' }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
