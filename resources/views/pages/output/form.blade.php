@extends('layouts.app')
@section('title', $formTitle)
@push('css')
{!!
    Helper::generate_css(array(
            'assets/plugins/custom/datatables/datatables.bundle.css'
        )
    )

!!}
@endpush
@section('content')
    <!--begin::Subheader-->
    <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">{{ $formTitle }}</h2>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Home</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Output Activity</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">{{ $formTitle }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid align-content-center">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                            height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path
                                    d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z"
                                    fill="#000000" opacity="0.3" />
                                <path
                                    d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z"
                                    fill="#000000" fill-rule="nonzero" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text pb-0 mb-0">
                    <ul>
                        <li>Pastikan Tiket yang anda buat sesuai dengan topik, forum dan kategoti tiket</li>
                        <li>Ukuran maksimal dokumen dapat yang diunggah adalah 2 MB per tiap unggahan dokumen</li>
                    </ul>
                </div>
            </div>
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{{ $formTitle }}</h3>
                            {{-- <div class="card-toolbar">
                                <div class="example-tools justify-content-center">
                                    <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                                </div>
                            </div> --}}
                        </div>
                        <!--begin::Form-->
                        <form class="form" id="form-output" method="POST" action="{{ url('/output/form') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{-- <input type="hidden" name="output_id" id="output_id" value="{{ (!empty($master)) ? $master->output_id : '' }}"> --}}
                            <div class="card-body">
                                <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Jobdesk Information:</h3>
                                <div class="mb-15">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Jobdesk : <span
                                                class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <select class="form-control select2" id="jobact_id" name="jobact_id"
                                                title="Plese select jobdesk activity" required>
                                                <option value="{{ !empty($master) ? $master->jobact->jobact_id : '' }}">
                                                    {{ !empty($master) ? $master->jobact->jobact_name : '' }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Department :
                                            <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <select class="form-control select2" id="department_id" name="department_id"
                                                title="Plese select department" required>
                                                <option
                                                    value="{{ !empty($master) ? $master->department->department_id : '' }}">
                                                    {{ !empty($master) ? $master->department->department_name : '' }}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="font-size-lg text-dark font-weight-bold mb-6">2. Output :</h3>
                                <div class="mb-15">
                                    <input type="hidden" name="output_id" id="output_id">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Output Activity
                                            :</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <input type="text" class="form-control" id="output_title" name="output_title"
                                                placeholder="Input jobdesk activity here"
                                                title="Input jobdesk activity here"
                                                value="{{ !empty($master) ? $master->output_title : '' }}"  />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Description :</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <textarea class="form-control summernote" name="output_desc" id="output_desc" rows="10"
                                                title="Please input output activity description here" >{{ !empty($master) ? $master->output_desc : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <div class="col-lg-11 col-md-11 col-sm-11 pull-right text-right">
                                            <button type="button" class="btn btn-outline-success font-weight-bold mr-2"
                                                id="btn-add-output"><i class="flaticon-file-1"></i> Add output</button>
                                        </div>
                                    </div>
                                    <div class="separator separator-dashed my-8"></div>
                                    <div class="from-group row justify-content-md-center">
                                        <div class="col-lg-10 col-md-10 col-sm-10">
                                            <table class="table table-separate table-head-custom table-checkable" id="output-list-table">
                                                <thead>
                                                    <tr>
                                                        <th>*</th>
                                                        <th>Title</th>
                                                        <th>Desc</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>64616-103</td>
                                                        <td>São Félix do Xingu</td>
                                                        <td nowrap="nowrap"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>64616-103</td>
                                                        <td>São Félix do Xingu</td>
                                                        <td nowrap="nowrap"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>64616-103</td>
                                                        <td>São Félix do Xingu</td>
                                                        <td nowrap="nowrap"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto pull-right text-right">
                                        @if (!empty($master))
                                            <button type="button" onclick="history.go(-1);"
                                                class="btn btn-light-primary font-weight-bold">Cancel</button>
                                        @else
                                            <button type="reset"
                                                class="btn btn-light-primary font-weight-bold">Cancel</button>
                                        @endif

                                        @if (empty($type) || $type == 'change')
                                            <button type="submit" class="btn btn-primary font-weight-bold mr-2"
                                                name="submitButton">Save</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
@endsection
@push('scripts')
    {!! Helper::generate_js([
        'assets/plugins/custom/datatables/datatables.bundle.js',
        'assets/js/pages/output/form.js'
    ]) !!}
@endpush
