<div class="modal fade" id="responsive-modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $formModalTitle }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-default-modal" method="POST" action="{{ url('permission/form') }}"
                enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="permission_id" id="permission_id"
                        value="{{ !empty($master) ? $master->permission_id : '' }}">
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Show <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="is_show" name="is_show"
                                title="Plese select" required>
                                <option value="{{ !empty($master) ? $master->setting_permission->param_id : '' }}">
                                    {{ !empty($master) ? $master->setting_permission->param_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Add <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="is_add" name="is_add"
                                title="Plese select" required>
                                <option value="{{ !empty($master) ? $master->setting_permission->param_id : '' }}">
                                    {{ !empty($master) ? $master->setting_permission->param_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Edit <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="is_edit" name="is_edit"
                                title="Plese select" required>
                                <option value="{{ !empty($master) ? $master->setting_permission->param_id : '' }}">
                                    {{ !empty($master) ? $master->setting_permission->param_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Delete <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="is_delete" name="is_delete"
                                title="Plese select" required>
                                <option value="{{ !empty($master) ? $master->setting_permission->param_id : '' }}">
                                    {{ !empty($master) ? $master->setting_permission->param_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Menu <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="menu_id" name="menu_id"
                                title="Plese select menu" required>
                                <option value="{{ !empty($master) ? $master->menu->menu_id : '' }}">
                                    {{ !empty($master) ? $master->menu->menu_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Role <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="role_id" name="role_id"
                                title="Plese select role" required>
                                <option value="{{ !empty($master) ? $master->role->role_id : '' }}">
                                    {{ !empty($master) ? $master->role->role_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
