<div class="modal fade" id="responsive-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $formModalTitle }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-default-modal" method="POST" action="{{ url('user/form') }}"
                enctype="multipart/form-data">
                <div class="modal-body" >
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" id="user_id"
                        value="{{ !empty($master) ? $master->user_id : '' }}">
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">First Name <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="user_first_name" name="user_first_name"
                                placeholder="Please input user first name here"
                                value="{{ !empty($master) ? $master->user_first_name : '' }}"
                                title="Please input user first name here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Last Name <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="user_last_name" name="user_last_name"
                                placeholder="Please input user last name here"
                                value="{{ !empty($master) ? $master->user_last_name : '' }}"
                                title="Please input user last name here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right">P.O.B:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="user_place_birth" name="user_place_birth"
                                placeholder="Please input place of birth"
                                value="{{ !empty($master) ? $master->user_place_birth : '' }}"
                                title="Please input place of birth" required />
                        </div>
                        <label class="col-md-2 col-form-label text-md-right">D.O.B:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="user_date_birth" name="user_date_birth"
                                placeholder="Please input date of birth"
                                value="{{ !empty($master) ? $master->user_date_birth : '' }}"
                                title="Please input date of birth" required readonly />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Email <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="email" class="form-control" id="user_email" name="user_email"
                                placeholder="Please input user email here"
                                value="{{ !empty($master) ? $master->user_email : '' }}"
                                title="Please input user email here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Password {!! !empty($master) ? '' : '<span
                            class="text-danger">*</span>' !!}</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="Please input user password here"
                                value=""
                                title="Please input user password here" {{ !empty($master) ? '' : 'required' }}  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Phone <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control only-number" id="user_phone" name="user_phone"
                                placeholder="Please input user phone here"
                                value="{{ !empty($master) ? $master->user_phone : '' }}"
                                title="Please input user phone here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Department <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="department_id" name="department_id"
                                title="Plese select department" required>
                                <option value="{{ !empty($master) ? $master->department->department_id : '' }}">
                                    {{ !empty($master) ? $master->department->department_name : '' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Avatar <span
                                class="text-danger">*</span></label>
                        <div class="image-input image-input-empty image-input-outline" id="kt_image_5"
                            style="background-image: url({{ !empty($master) ? asset($master->user_avatar) : asset('assets/media/users/blank.png') }})">
                            <div class="image-input-wrapper"></div>

                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                <i class="fa fa-pen icon-sm text-muted"></i>
                                <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="profile_avatar_remove" />
                            </label>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
