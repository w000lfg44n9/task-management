<div class="modal fade" id="responsive-modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $formModalTitle }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-default-modal" method="POST" action="{{ url('menu/form') }}"
                enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="menu_id" id="menu_id"
                        value="{{ !empty($master) ? $master->menu_id : '' }}">
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Menu Name <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="menu_name" name="menu_name"
                                placeholder="Please input menu name here"
                                value="{{ !empty($master) ? $master->menu_name : '' }}"
                                title="Please input menu name here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Description <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="menu_desc" name="menu_desc"
                                placeholder="Please input decription here"
                                value="{{ !empty($master) ? $master->menu_desc : '' }}"
                                title="Please input decription here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right">Menu URL</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="menu_url" name="menu_url"
                                placeholder="Please input menu url"
                                value="{{ !empty($master) ? $master->menu_url : '' }}"
                                title="Please input menu url" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right">Menu Icon</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="menu_icon" name="menu_icon"
                                placeholder="Please input menu icon"
                                value="{{ !empty($master) ? $master->menu_icon : '' }}"
                                title="Please input menu icon" required />
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Parent Menu <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control select2" id="menu_parent_id" name="menu_parent_id"
                                title="Please select menu" required>
                                <option value="{{ !empty($master) ? $master->menu_parent->menu_id : '' }}">
                                    {{ !empty($master) ? $master->menu_parent->menu_name : '' }}</option>
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Order <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control only-number" id="menu_order" name="menu_order"
                                placeholder="Please input menu order"
                                value="{{ !empty($master) ? $master->menu_order : '' }}"
                                title="Please input menu order here" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-2 col-md-2 col-sm-12">Menu Slug <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="menu_slug" name="menu_slug"
                                placeholder="Please input menu slug"
                                value="{{ !empty($master) ? $master->menu_slug : '' }}"
                                title="Please input menu slug here" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
